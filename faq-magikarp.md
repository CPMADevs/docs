# CPMA Commands/Settings FAQ

*Hello! This is Magikarp�s CPMA Commands/Settings FAQ! This is for those same old questions that are often brought up regarding console commands and other sorts of technical CPMA-related questions. Also, please try go through the CPMA settings/setup to see if you can try to find what you need there first. Feel free to contact me on Discord @Magikarp to suggest any questions or improvements I could make. If after reading this FAQ you have not found the answer to your problem, feel free to ask away in the #help channel of the CPMA Discord. Also be sure to check out the pinned messages for anything that might be helpful!*

## General

**How come my settings aren�t being saved?**

* If you are using DEZ�s installer, I would recommend doing `/writeconfig my` as he utilizes autoexec to execute the my.cfg full of your settings. Otherwise you are free to do `/writeconfig autoexec` or name it something else as long as your autoexec.cfg will execute that config.


**How do I bind a key to a command?**
* Use `/bind x [command]`, *x* being whichever key you wish to bind it to.

**How do I change my max fps?** 
* Use `/com_maxfps x`. 

**How do I change my sensitivity? Also I played TF2/CSGO/L4D2, can I use the same   sensitivity in this game and it will feel the same?**
* Use the command `/m_speed x`, *x* being your sensitivity value. Also yes, I was told that it�s essentially 1:1 CPMA to Source games.

**How come I am unable to load certain maps (such as b0_beta6)?**
* Try using `/com_hunkmegs 512`.

**How can I skip the id software intro/movie?**
* Try using these two commands `/com_skipIdlogo 1` and `/com_introplayed 0`.

**How do I stop the Enter CD Key Screen from popping up?**
* Download [this file](http://puu.sh/rAjdA/43d95a5f48.rar). Extract the file to your baseq3 folder and you�ll never have the problem again. This will also fix possible issues of joining servers and being instantly redirected to the main menu.

## Customization/Gameplay

**I can�t tell who is my teammate! How do I make them the same model and colour?**
* Use `/cg_forcemodel 1`. This will make your team use the same model as you. For the same colour, use `/cg_forcecolors 1`, this will make them the same colour as you.

**How do I change the player enemy model(such as tankjr)**
* The command for this is `/cg_enemymodel tankr/pm` Adding a `/pm` at the end will enable the use of the Promode models (which is highly recommended) so you can set the colour of the enemy model. This will also make the enemy model the same model in team modes.

**How do I change the colour of the enemy model(such as completely green)?**
* The command for this is `/cg_enemycolors iiiii`. This requires the a Promode enemy model and make the enemy model the same colour in team modes. As for different colours other than green, I will later on try to provide more information on how to use more different colours.

**How do I change my fov?**
* The command is `/cg_fov x`, *x* being whatever value you wish to use. I believe that 130 is the max possible fov that has a visual difference with 50 being the lowest.

**How do I turn my viewmodel off?**
* Use the command `/cg_drawgun 0`. 1 turns it on, and 2 will turn it on and make it not sway.

**How do I make it so that I won�t be able to switch to a weapon with no ammo?**
* Try this command `/cg_noAmmoChange 1`

**How do I fix my spectate? Sometimes it won�t let me switch to a player�s POV.**
* That is currently a bug in CPMA which qrealka fixed for the next release, for now use `/follownext` to fix it.


## Video/Graphics

**How do I change my resolution?**
* You will need to use `/r_mode`, you can use `/modelist` to see which `r_mode` will give what resolution or use `-1` so you can set a custom resolution yourself with `/r_customwidth` and `/r_customheight`.

**How do I make my game look low graphics-like and have flat textures?**
* You will have to use the `/r_picmip` command. For the absolutely lowest of graphics and flat textures use `/r_picmip 16`�, the usable values range from `0-16`, with `0` being the absolutely highest of graphics. After inputting this command, you will need to do a `/vid_restart` to see the changes.

**How do I change my brightness?**
* Use `/r_gamma x`, where x should be a value between `0.5-3`.

## HUD

**How can I change my hud or look for other huds?**
* If you are using DEZ�s CPMA installer, under cpma/hud you can find various hud files and screenshots of each hud. To actually change the hud, the command is `/ch_file �hudfilename�`. 

**How can I edit/create my own hud?**
* Aside, editing values by text in the config file, you can actually use SuperHudEditor to easily move hud elements around and even disable them. If you already have DEZ�s installer, superhudeditor is already included, otherwise you can [download it here](https://cdn.discordapp.com/attachments/180881600324567041/257773629595582464/superhudeditor-0.3.0.rar). 

**How come my SuperHudEditor keeps crashing the instant I close the tips?**
* No clue, but one current solution is to go to this directory:
	* Windows 7: appdata/roaming/superhudeditor/superhudeditor.conf

* Or you can use Windows `Key+R` and type in `%appdata%` to get there faster.
	* Open up the `.conf` file in Notepad and add this line to it and save it: `startup_checkforupdate=false`. That should be able to solve the issue.

## Demos

**How do I record demos?**
* There are multiple values to use for the `/cg_autoAction` command that lets you not only record demos, but have end of game screenshots and stats. For just pure demos, you can just use `/cg_autoAction 4`, but if you�d like you can use `/cg_autoAction 7` as it is the one that has been recommended for a tournament setting and will give end of game screenshots and stats.

**How do I change my demo files ending with .71 to .68 faster?**
* Kimi made this .bat to do so, [download the file.]( https://cdn.discordapp.com/attachments/114018075937865732/257173608395309071/71to68.bat) 

---
**Credits**
* Benroads for the q3key.rar download link.
* DEZ for having most if not all of the commands and their descriptions in the docs folder that no one reads.
* GSBL4CK for finding the solution to fix superhudeditor.
* Kimi for the *71to68.bat* file to mass convert demo files ending with .71 to .68.
* nifwl for having his config also explain stuff, also those sweet intro skipping commands.