# CNQ3 Client Migration

This is a quick overview of some stuff that's new or different in *CNQ3* 1.49 vs previous *CNQ3* versions and/or the original *Quake 3* engine. For full details, please check out the [CNQ3 Client Settings](cnq3-client-settings) guide.

General behavior differences to watch out for:
* CNQ3 does **not** write to q3config.cfg on exit. Use `writeconfig` manually to save to a file.
* `m_speed` replaces `sensitivity`.
* `r_mode` works differently.
* `r_width` replaces `r_customwidth`.
* `r_height` replaces `r_customheight`.
* `r_brightness` replaces `r_overBrightBits`.
* `r_mapBrightness` replaces `r_mapOverBrightBits`.
* `r_ignorehwgamma` was removed because gamma correction is always available.
* `cl_allowDownload 1` doesn't use the original (super slow) download system.


## Variables

|Variable|Purpose|
|:---|:---|
|`con_scale`|Console text scale|
|`con_scaleMode`|Console text scaling mode (scale with res, don't scale with res, fixed size)|
|`con_completionStyle`|Console auto-completion mode (standard, ET-style)|
|`con_history`|Saves the console command history to a file|
|`con_drawHelp`|Draws a help panel below the console|
|`cl_allowDownload`|Specifies the download system (id, CNQ3, disabled)|
|`r_monitor`|Specifies what screen to display the game on (primary or a given monitor)|
|`r_mode`|Full-screen display mode (desktop res, custom res with engine upscaling, custom res with screen/drivers upscaling)|
|`r_blitMode`|Full-screen image upscaling mode (aspect-ratio preserving, stretched, no upscale)|
|`r_width`|Custom rendering width (replaces `r_customwidth`)|
|`r_height`|Custom rendering height (replaces `r_customheight`)|
|`r_msaa`|Anti-aliasing sample count|
|`r_brightness`|General brightness (replaces `r_overBrightBits`)|
|`r_mapBrightness`|Map brightness (lightmap textures only, replaces `r_mapOverBrightBits`)|
|`r_ext_max_anisotropy`|Anisotropic filtering taps|
|`s_autoMute`|When to mute automatically (minimized, unfocused, never)|
|`m_speed`|Mouse sensitivity (replaces `sensitivity`)|
|`m_accelStyle`|Selects what mouse accel formula/system to use|
|`m_accelOffset`|Mouse acceleration offset for the power function|
|`in_noGrab`|Disables input grabbing|
|`in_mouse`|Selects the mouse input mode (raw, win32, disabled, ...)|
|`in_minimize`|Windows only: registers a hotkey to minimize/restore the game|


## Commands

|Command|Purpose|
|:---|:---|
|`help` \<cvar\|cmd>|Prints help for the given cvar or command|
|`searchhelp` \<pattern>|Lists all cvars and commands whose names or help text contains the pattern|
|`screenshotnc`|Takes TARGA (.tga) screenshot with the console hidden|
|`screenshotncJPEG`|Takes a JPEG (.jpg) screenshot with the console hidden|
|`dlpak` <checksum>|Downloads a pak by its checksum if the pak doesn't exist locally|
|`dlmap` \<map_name>|Downloads a map by name if no map with such a name exists locally|
|`dlmapf` \<map_name>|Downloads a map by name|
|`dlstop`|Cancels the map/pak download in progress, if any|