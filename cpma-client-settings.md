# CPMA Client Settings

Date: 22-Apr-2009

This document explains some of the client-side support for CPMA. It will
never be as current as the changelog, so you should always check that
with each new release.


## Network Settings

* `cg_lagHax <-1|0> (default: -1)`

    A combination of adaptive prediction and an updated version of the
    famous "50ms hack" we introduced way back in 99v6 that also does small
    amounts of lag compensation. Capped at 100ms no matter what: this is
    intended solely to make European / EastUS v WestUS / etc games a bit
    less of a hassle, not to hack dialup players into aimgods at the expense
    of everyone else.
    
    `0` : Disables it.
    
    `-1` : Means "as much as I'm allowed."

    It's naturally adaptive. You'll lose some of your "feel" for lag, which
    messes up your RL aim, etc. This doesn't suffer from the CS/etc problems
    of "total BS" shots that piss everyone off; it's not trying to be some
    panacea for modemers; and I'm honest enough to call it the hack that it
    is instead of pretending that it magically makes lag suddenly not exist,
    but all in all it's a pretty nice end result. If you use this, any form
    of nudging will generally make you LESS accurate if your ping's under
    100ms, because it'll screw up the adaptive calculations.

* `cg_nudge <value> (default: 0)`

    An updated and much improved version of
    id's crippled cl_timenudge. Allows you to use nudges beyond -25, and
    automatically adjusts them to your ping: if you use -50 with a 20 ping,
    you get -20. If you spike to 40ms for a few seconds, you get -40 during
    the spike. This give you a "consistent worldview" that cl_timenudge
    can't, and generally helps regardless of your connection.

* `cg_optimiseBW <bitmask> (default: 1)`

    `1` : Server-to-client.

    Significantly reduces the amount of non-critical data sent to you.
    Regrettably, this also makes you unable to see players through portals,
    thanks to a bug in the Q3 engine. Small price to pay though for the HUGE
    difference it makes to team games. Servers can, and by default do, force
    this on for all clients. It's probably worth setting it to 1 anyway
    though, just in case you end up on a server that's changed it to 0.
    
    `2` : Client-to-server.

    Use this if your connection is UTTERLY starved for upstream bandwidth
    (i.e. from you TO the server). Essentially, if you're on dialup or one
    of those Belgian Warp connections. Understand that if you're warpy and
    you choose to NOT set this because you like the advantage you get from
    warping, you're screwing YOURSELF. Your shots will end up with
    potentially huge random delays on them, so even if you're LPB the server
    may not see that you fired until up to 100ms after the fact, effectively
    making your weapons act like you have an unstable and much laggier
    connection, and without cg_nudge's ability to smooth it out.

* `cg_predict <0|1|2> (default: 1)` (Replaces cg_nopredict)

    `0` : Off.

    `1` : Normal.

    `2` : Optimised.
      
    The normal prediction path is extremely slow at times (notably around curves)
	and can cost you 100fps on a GHz machine. This new scheme is MUCH faster,
	but slightly more prone to errors. Oddly enough, it's still more accurate than
	the original id prediction code (i.e. before the CPMA fixes).
	If you have a slow machine, it's definitely worth trying.
	Note that cg_predict 2 was introduced 9 Sep 2002, the definition of a
	"slow machine" has changed since then.
	Today's computers will not notice any difference at all.

    **Note**: Do not use cg_predictItems 1 with cg_predict 2.

* `cg_predictItems <0|1> (default: 1)`

   Toggle client-side item prediction. 0 option to not do local prediction
    of item pickup. If you get many false pickups (due to lag, packetloss or
    high ping) you should definitely use 0. It's annoying when the client
    predicts that you picked up RL, only to notice a bit later that you did
    not pick up anything.
    
   **Note**: Do not use cg_predictItems 1 with cg_predict 2.

* `cg_smoothClients`

   Does not exist in CPMA -- see `cg_xerpClients` instead.

* `cg_xerpClients <-1|0|1> (default: 1)`

   A replacement for id's cg_smoothclients.
   
    `-1` : Hacked extrapolation. Intended for HPBs. This smooths players out when you use high timenudges, at the cost of some accuracy. It's typically easier to hit a smooth target that's a few pixels misplaced than it is to hit one that looks like it's teleporting all over the map, so this combined with cg_nudge is the best option for HPBs.
    
    `0` : No extrapolation. Fine if you're a LPB.

    `1` : id's smoothclients.

  Fine if you have cg_nudge 0, worthless otherwise. Note that prediction errors, such as players in walls, are likely to occur. Tends to increase how much other players warp to you. Use at your own risk.


## CPMA Colors

CPMA uses `a-y` to display colors, with `a-x` being an HSV table and y being white. Colors can be used in: 

* \name
* \nick
* \ch_crosshairText
* \say_team
* \say_teamnl
* \ch_crosshairColor

Note these colors do NOT apply to the SuperHud which uses rgba


## Chat Tokens

You can use any number of chat tokens in binds. Would recommend adding `[#H/#A] #U` to all binds where team mates need to know your current condition - for instance `"say_team ^3Need Weapon! ^5[#H/#A] #U"`. But do NOT overdo it as excessive use of tokens can easily cause information overload. For instance, CTF high/low binds should be as simple as possible. The location of EFC is of most importance and your own health/armor/weapon/pu/etc nearly irrelevant.

|Token|Description|
:-------|:----------| 
|`#A`| Armor. Current Armor level. `#a` (lower case) does not change color according to Armor level.<br>*Example*: `say_team "Hurting bad - #H/#A"`|
|`#C`| Corpse. The location where you last died.<br>*Example*: `say_team "Overrun at #C"`|
|`#D`| Damaged by. The last enemy to score a hit on you.<br>*Example*: `say_team "#D is heavily armed! RUN AWAY! RUN AWAY!"`|
|`#E`| Enemy Presence. Detailed information on all enemies in your FOV.<br>*Examples*: "2 enemies", "EFC", "Enemy QUAD plus 1", etc.|
|`#F`| Nearest Friendly Player's \name. Not the same as `#N` which uses \nicks when available. It's recommended to use `#N` instead of `#F`.<br>*Example*: `bind space "drop; say_team Dropped weapon for #F"`|
|`#H`| Health. Current Health level. `#h` (lower case) does not change color according to Health level.<br>*Example*: `say_team "Hurting bad - #H/#A"`|
|`#I`| Nearest Item. Shows the nearest "significant" (weapon, armor, powerup, or MH) available item, including dropped items.<br>*Example*: `say_team "#I available"`|
|`#L`| Location. Many maps have terrible target_location entities e.g. PG on PRO-Q3DM6 shows as YA. This shows the nearest "significant" item spawn (weapon, armor, powerup, flag or MH), whether the item is there or not.<br>*Example*: `say_team "Took/waiting for #L"`|
|`#M`| Ammo Wanted. Lists all types of ammo for weapons you have that are empty or nearly so.<br>*Example*: `say_team "Need #M"`|
|`#N`|Nearest Friendly Player's \nick. Uses your nearest team mate's \name should he not have set his \nick. It's recommended to use `#N` instead of `#F`.<br>*Example*: `bind space "drop; say_team Dropped weapon for #N"`|
|`#P`| Last Pickup.<br>*Example*: `say_team "Took #P"`|
|`#S`| Item in Sights. Item that you are aiming at directly. Distance to the item is irrelevant.|
|`#T`| Target. The last enemy you hit.<br>*Example*: `say_team "#T is weak - finish him!"`|
|`#U`| PowerUps. Powerups you carry - Includes flags.|
|`#V`| Victim. The last enemy you killed.|
|`#W`| Weapon. `#w` (lower case) does not change color according to Ammo level. Lists the current weapon and ammo you have.<br>*Example*: `say_team "Need weapon! I only have #W"`|


## Player Models & Colors CVars

* `color <CHBLS> (default: "l777o")` 

    Colour string characters 0-9 and A-Z.
   
    The CHBLS characters in order:
    1. Rail core color.
    2. Head/Helmet/Visor color.
    3. Body/shirt color.
    4. Legs color.
    5. Rail spiral color.
    
    This also affects your team mates' color if you use `cg_forceColors 1`.

* `cg_deadBodyDarken <0|1> (default: 1)`

     Darkens players as soon as they become corpses.

* `cg_enemyColors <CHBLS> (default: "g2222")`

    Formatted as CHBLS, same as \color. Requires a PM or FB `cg_enemymodel` e.g. `cg_enemyModel sarge/pm`.

* `cg_enemyModel <modelname> (default: "keel/pm")`
   
     Setting this will force all players on the enemy team to appear to have this model. PM model highly recommended.

* `cg_forceColors <0|1> (default: 1)`
   
     Force your team to use the same color as you. PM model and `cg_forceModel 1` recommended.

* `cg_forceModel <0|1> (default: 1)`

    Force your team to use the same model as you. PM model and `cg_forceColors 1` recommended.

* `cg_showPlayerLean <0|1> (default: 1)`

    Allows the disabling of viewed model leaning that was introduced in the 1.27x patches. 0 is more accurate in regards to hitboxes.
  
* `model <modelname> (default: "mynx/pm")` 

    Your model. It is highly recommended that you choose a PM model. This also affects your team mates' models if you use `cg_forceModel 1`.

* `cg_redTeamColors <CHBLS> (default: "cccab")`

    Red team player colors.

* `cg_blueTeamColors <CHBLS> (default: "mmmpo")`

    Blue team player colors.

* `cg_forceTeamColors <bitmask> (default: 15)`

    Decides when `cg_redTeamColors` and `cg_blueTeamColors` are used.

	`1` - enable for your own team when in first person.

	`2` - enable for the enemy team when in first person.

	`4` - enable when in free-float camera mode.

	`8` - only enable for game types with flags.

* `cg_forceTeamModel <0|1> (default: 0)`

    Decides if your teammates use the model specified by `cg_teamModel`.

* `cg_teamModel <modelname> (default: "sarge/pm")`

    The model of your teammates.


## Audio CVars

* `cg_ammoWarning <0|1> (default: 1)`

    Toggles the "click" sound. The visual effect can be changed in your SuperHUD config.

* `cg_nochatbeep <0|1> (default: 0)`

    Allows client to suppress "beeps" heard during normal chat messages. Especially convenient when connected to spam-riddled GTV servers.

* `cg_nohitbeep <0|1> (default: 0)`

    Disable hit beep. Some like it for LG.

* `cg_noTaunt <0|1> (default: 1)`

    Disable all taunts, not just voicechat ones.

* `cg_noteamchatbeep <0|1> (default: 0)`
 
     Allows client to suppress "beeps" heard during team chat messages.

* `cg_oldCTFSounds <0|1|2> (default: 2)`

	`0` - TA sounds, with voiceovers on captures, flag pickups, etc.

	`1` - Near-worthless Q3 1.17 sounds, same for both teams.

	`2` - Team-specific sounds, no voiceovers.

	Also controls non-leadchange FTDM end-of-round announcements.

* `s_ambient <0|1> (default: 1)`

    Disable ambient sounds like the gongs on q3wcp2 and the void/wind/water background noise on a ton of maps.

* `s_announcer <string> (default: feedback)`

    Set to hellchick for alternative game feedback sounds - requires `\snd_restart`. If using CNQ3, you can set `s_announcer ""` to remove all announcer sounds. Not recommended for original quake3.exe which plays "sound/feedback/hit.wav"...

* `cg_fragSound <string> (default: "0")`

	Plays a sound after a kill.

	`0` - No sound

	`1` - Tonal impact playing the note D

	`2` - Tonal impact playing the note E

	`3` - Tonal impact playing the note F#

	`4` - Tonal impact playing the note G

	`5` - Cork pop

	`6` - Cash register

	`7` - Grappling hook impact

	Leave at `0` or empty for no sound or specify a sound file path including the file extension.

	Example: `\cg_fragSound "sound/pure3/grapple/dink.wav"`


## Demo Playback CVars

* `cg_demoMapOverrides <0|1> (default: 1)`

	Allows the new demo player to display another map.

	The override list is loaded from "cfg-maps/demomaps.txt", where each line has the format "DemoName DisplayName", e.g. "cpm3a cpm3b_b1" will display cpm3b_b1 instead of cpm3a.

* `cg_demoSkipPauses <0|1> (default: 0)`

	Instructs the new demo player to skip server pauses.

* `cg_demoUI <0|1> (default: 1)`

	Draws the demo player's user interface.

* `demoname <string> (default: "")`

	Path to .wav file for demo shoutcasting.

	The path must not contain the file extension.
	The sound will be played at match start.
	If the sound file has the same name as the demo and sits in the same folder, this CVar will be set to point to it when loading the demo from the UI.
 
* `cg_thirdPerson <0|1> (default: 0)`

	Enables third-person view.

	Third-person view can only be used during demo playback.
 
* `cg_thirdPersonAngle <0 to infinite> (default: 0)`

	Yaw angle in degrees.

	Third-person view can only be used during demo playback.
 
* `cg_thirdPersonRange <40 to 999> (default: 40)`

	Distance to the player.

	Third-person view can only be used during demo playback.


## General CVars

* `cg_altLightning <string> (default: "233")`

    `0` : Original (pre-TA) id LG beam.

    `1` : CPMA lightning.

    `2` : Thin lightning.

    `3` : Lightning using 1.44 render.

    Switch between various LG shaft visuals e.g. `cg_altLightning 032`.

    Characters in order:
    1. Player lightning.
    2. Enemy lightning.
    3. Team lightning.

    First character is default for enemy and team mate lightning if not specified.

* `cg_altPlasma <0|1> (default: 0)`

    `0` : Original id plasma.
    
    `1` : CPMA plasma.

* `cg_autoAction <bitmask> (default: 0)`

     Perform game actions such as demo recording and screenshots.
    
    `1` : Save stats to a local text file at the end of a match. Logs are stored in `<cpma_root>/stats/<date>/<logname>.txt`.

    `2` : Take an end-of-game screenshot.

    `4` : Record a demo of the game. Requires warmup to be on, i.e. `\ready`. It will NOT trigger if you join a game that has already started (or you got disconnected). In those cases you should either use `\autorecord` or set value `128`.

    `8` : Multiview the game. This also works for GTV even if left unattended! PLEASE use it GTV admins. If put in gtv.cfg the gtv just has to be connected to the q3server, there is NO need for a camera guy as the MV will start once players ready up!

    `16` : Only do these things if you're actually playing in the game.

    `32` : Follow power up.

    `64` : Follow killer.

    `128` : Record a demo even if you joined mid-game. Requires `4` to be set as well.

* `cg_brasstime <value> (default: 2500)`

    Controls the time shell casings being discharged from MG+SG are visible.

* `cg_damageDraw <0|1> (default: 1)`

    Obscure player's vision with blood effect when they are hit.

* `cg_drawBrightSpawns <0|1> (default: 1)`

    Draws spawn points with bright shaders. Makes learning spawns much easier.

* `cg_drawBrightWeapons <bitmask> (default: 0)`

    Displays weapon models with a fullbright shader.

	 `1` - First-person, your own gun.

	 `2` - First-person, carried by teammates.

	 `4` - First-person, carried by enemies.

	 `8` - Weapons lying on the map.

	`16` - Free-float camera, carried by players.

* `cg_drawGun <0|1|2> (default: 2)`

    Toggles the gun being drawn.
    
    `0` - Gun off.

    `1` - Gun on.

    `2` - Gun on, no sway.

* `cg_fallKick <0|1> (default: 1)`

    Toggles the screen bouncing when player falls.

* `cg_fov <1.0 to 150.0> (default: 102)`

	Horizontal field of view. Higher values give better peripheral vision while lower values give better frontal vision.

	The real limit is `130` when not using a client that has depth clamping enabled. To enable it in CNQ3, use `r_depthClamp 1`.

* `cg_zoomfov <1.0 to 150.0> (default: 22.5)`

	Horizontal field of view when zoomed in. Higher values give better peripheral vision while lower values give better frontal vision.

	The real limit is `130` when not using a client that has depth clamping enabled. To enable it in CNQ3, use `r_depthClamp 1`.

* `cg_gibs <0|1> (default: 1)`

    Gibs and blood splatter effect.

* `cg_gunOffset <x,y,z> (default: "5,0,0")`

    Moves gun along x,y,z axis. Valid x,y,z values are -9 to 9, for instance `cg_gunOffset 5,-9,9`.

* `cg_itemFX <bitmask> (default: 7)`

    Control the cutesy gimmicks on `cg_simpleitems 0` items.  
    With `cg_simpleitems 1` the only difference is flags as they have no simple model.
    
    `1` - Bob up and down.
    
    `2` - Rotate (asymmetric items will always rotate).
    
    `4` - Scale up on respawn.

* `cg_lightningImpact <0 to 2> (default: 0)`

	Draws the lightning gun's impact mark.

	`0` - Disabled.

	`1` - Impact marker only.

	`2` - Impact marker and sparks.

* `cg_marks <value> (default: 2500)`

    Milliseconds marks after explosions are visible.

* `cg_muzzleFlash <0|1> (default: 1)`

    Specifies if there is a muzzle flash when gun is fired.

* `cg_mvSensitivity <0 to 100> (default: 1)`

    Mouse sensitivity in the live multi-view coach UI.
	This is only used with engines that aren't compatible with the cgame input forwarding extension.
	During demo playback, `ui_sensitivity` is used instead.

* `cg_noAmmoChange <0|1|2> (default: 1)`

    Disables the ability to switch to a weapon that doesn't have any ammo. Useful for multiple-weapon binds.
    
    `0` - When out of ammo and firing, switches to the next weapon. Can't select a weapon with no ammo.
    
    `1` - When out of ammo and firing, switches to the next weapon. Can select a weapon with no ammo.
    
    `2` - When out of ammo and firing, keeps the current weapon. Can select a weapon with no ammo.

* `cg_nomip <bitmask> (default: 1023 -> all have r_picmip 0)`

    Allows changing graphics to picmip 0 setting, regardless of the current `r_picmip` value. Changes require `vid_restart`.
    
    `1` - Lightning.
    
    `2` - Plasma.
    
    `4` - Rocket and Grenade explosions.
    
    `8` - Grenades (the grenade ITSELF, not the same as 4).
    
    `16` - Bullets (machinegun and shotgun).
    
    `32` - Railgun.
    
    `64` - BFG.
    
    `128` - Blood.
    
    `256` - Smoke.
    
    `512` - Rockets (the projectile, not the same as 4).

* `cg_noProjectileTrail <0|1> (default: 0)`

    Removes underwater bubble trails from weapon fire.

* `cg_railstyle <0 to 7> (default: 3)`

    Changes the style of the rail trail.

    `0` - No trail.
    
    `1` - Straight core (line) only.
    
    `2` - Spiral only, no core.
    
    `3` - Spiral with straight core.
    
    `4` - Dotted core only.
    
    `5` - Original id style.
    
    `6` - Spiral with dotted core.
    
    `7` - Everything. 

* `cg_railCoreWidth (default: 2)`

    Width of the Core. BaseQ3 r_rail* cvars are now obsoleted.

* `cg_railRingWidth (default: 8)`

    Width of the Rings. BaseQ3 r_rail* cvars are now obsoleted.

* `cg_railRingStep (default: 32)`

    Distance between Rings. BaseQ3 r_rail* cvars are now obsoleted.

* `cg_railTrailTime (default: 400)`

    Rail time in milliseconds. BaseQ3 r_rail* cvars are now obsoleted.

* `cg_shadows <0|1> (default: 1)`

    Player shadows.

* `cg_simpleitems <0|1> (default: 0)`

    Simple 2D items.

* `cg_smoke_SG <0|1> (default: 1)`

    Controls the smoke on the shotgun blast.

* `cg_smokeRadius_GL <value> (default: 4)`

    Controls the size of the smoke trail for grenades.

* `cg_smokeRadius_RL <value> (default: 6)`

    Controls the size of the smoke trail for rockets.

* `cg_trueLightning <0.0 to 1.0 and negative> (default: 1)`

    `0` - Default LG feedback as seen in baseq3 (sways).
    
    `1` - Pure client side rendering of LG graphic.
    
    Fractional values - Mix between server and client rendering of LG.
    
    Negative values - No LG graphic.

* `cg_useScreenShotJPEG <0|1> (default: 1)`

    Whether cg_autoaction 2 uses tga or jpg format.

* `cg_viewAdjustments <0|1> (default: 0)`

    Replaces and unifies cg_run* and cg_bob* which are all deleted.

* `cg_zoomAnimationTime <0 to 500> (default: 150)`

    Sets the zoom duration in milliseconds.

* `ch_consoleLines <1 to 64> (default: 3)`

    Maximum line count shown by the 'Console' SuperHUD element.

* `ch_drawKeys <bitmask> (default: 0)`

    Enables/disables the new KeyDown/Up_<key> SuperHUD elements.

	`1` - When you're playing.

	`2` - When you're following another player.

	`4` - During demo playback (won't work with pre-1.50 demos).

* `ch_playerNames <0|1> (default: 0)`

    Display player names above model heads. Only works during demo playback.

* `com_blood <0 to 2> (default: 1)`

	Shows blood when players are hit.

	`0` - No blood.

	`1` - id's original blood.

	`2` - Terifire's improved blood.

* `ui_sensitivity <0 to 100> (default: 1)`

    Mouse sensitivity in the UI.

    With compatible engines, this is also the sensitivity in the multi-view coach UI.  
    With other engines, the only scaling available is in the live multi-view coach UI through `cg_mvSensitivity`.

* `mvw_DM <x y w h> (default: "480 48 160 120")`

	PiP window coordinates for the opponent in 1v1

	Value format: \<Left Top Width Height\> in a 640x480 coordinate system.  
	Only available in multi-view mode and when not in coach view.  
	You can leave the CVar's value as empty to disable the view.

* `mvw_TDM1 <x y w h> (default: "")`

	PiP window coordinates for teammate #1

	Value format: \<Left Top Width Height\> in a 640x480 coordinate system.  
	Only available in multi-view mode and when not in coach view.  
	You can leave the CVar's value as empty to disable the view.

* `mvw_TDM2 <x y w h> (default: "")`

	PiP window coordinates for teammate #2

	Value format: \<Left Top Width Height\> in a 640x480 coordinate system.  
	Only available in multi-view mode and when not in coach view.  
	You can leave the CVar's value as empty to disable the view.

* `mvw_TDM3 <x y w h> (default: "")`

	PiP window coordinates for teammate #3

	Value format: \<Left Top Width Height\> in a 640x480 coordinate system.  
	Only available in multi-view mode and when not in coach view.  
	You can leave the CVar's value as empty to disable the view.

* `mvw_TDM4 <x y w h> (default: "")`

	PiP window coordinates for teammate #4

	Value format: \<Left Top Width Height\> in a 640x480 coordinate system.  
	Only available in multi-view mode and when not in coach view.  
	You can leave the CVar's value as empty to disable the view.

* `cg_errordecay <0 to 200> (default: 100)`

	Duration, in milliseconds, over which client-side mispredictions are smoothed out.

* `cg_teamChatsOnly <0|1> (default: 0)`

	Redirects global chat to the 'Console' SuperHUD element.

	`0` - Global chat goes to the 'Chat' SuperHUD element

	`1` - Global chat goes to the 'Console' SuperHUD element

	Team chat always goes to the 'Chat' SuperHUD element.

* `cg_stereoSeparation <float> (default: 0.4)`

	Left/right view offsets.
	
	Requires stereoscopic rendering to be enabled on the client.

* `cg_newModels <0|1> (default: 1)`

	Enables deft's improved 3D models.

* `cg_animateChatBalloon <0|1> (default: 1)`

	Enables a looping animation of the chat sprites.

* `cg_weaponConfig_default|none|gauntlet|MG|SG|GL|RL|LG|RG|PG|BFG|hook <string> (default: "")`

	These are per-weapon config CVars executed upon weapon switch.

	`cg_weaponConfig_default` is used when the corresponding weapon config CVar is empty.

	`cg_weaponConfig_none` is used when there isn't exactly 1 equipped first-person weapon. This happens when in a free-float camera view or in Multi-View.

* `cg_modeConfig_default|1v1|2v2|CA|CTF|CTFS|DA|FFA|FT|HM|NTF|TDM <string> (default: "")`

	These are per-mode config CVars executed when the game mode changes.

	`cg_modeConfig_default` is used when the corresponding mode config CVar is empty.

* `cg_teamConfig_free|red|blue|spec <string> (default: "")`

	These are per-team config CVars executed when the followed team changes.

* `cg_mapConfig_default <string> (default: "")`

	The fallback map config used when the corresponding map config CVar (`cg_mapConfig_<mapName>`) is empty or undefined.

	Example for cpm3a: `cg_mapConfig_cpm3a` is used when not empty, `cg_mapConfig_default` otherwise.

* `cg_zoomSensitivity <0.0 to 100.0> (default: 1)`

	Scales the mouse sensitivity when zoomed in.

	To match the behavior of older CPMA versions, set it to `1`.

* `ui_swapMouseButtons <0|1> (default: 0)`

	Swaps the left and right mouse buttons in the UI.

	It applies to the main menu, the in-game menu and the multi-view demo playback UI.
	The live multi-view UI still uses +attack instead of a specific key or button.
