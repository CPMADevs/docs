# Dealing with Config Files

This guide assumes the engine used is CNQ3 but is valid no matter the mod.

## Basic concepts

Config structure, loading and saving:
* Configuration files have the `.cfg` extension and contain one command per line.
* Lines that are empty or start with "//" are ignored by the engine.
* Loading a config is done with the `exec` command. It executes each line as a command as if it were typed at the console.
* Writing a config is done with the `writeconfig` command. It writes commands to set variables and bind actions to keys.

Configs written by the engine (`writeconfig`) contain 2 types of information:
1. The value of variables, e.g. `seta r_fullscreen "1"` to enable full-screen mode.
2. The list of key/action bindings, e.g. `bind space "+moveup"` for jumping with the space bar.

Every mention of a config file in this guide assumes the file sits in the mod's directory. For CPMA, the config files would be in `$(Q3_ROOT)/cpma`.

## Default behavior

When CNQ3 starts up, it will:
* `exec default.cfg` (in mod folder or `baseq3`)
* `exec q3config.cfg` (in mod folder only)
* `exec autoexec.cfg` (in mod folder only)

When the CNQ3 1.50+ client shuts down, it will:
* `writeconfig q3config.cfg`

If you don't change anything, your up-to-date and automatically loaded/saved config file will be `q3config.cfg`.

Note that older CNQ3 clients didn't write back to `q3config.cfg` on exit, meaning that you were forced to either use `writeconfig` or edit your config file manually whenever you wanted to make changes.

## Recommended set-up

This is the recommend way of dealing with configs:
* Give your config a name that is **not** `default.cfg`, `q3config.cfg` or `autoexec.cfg`. Let's call our example config `my.cfg`, where "my" could be replaced by e.g. your player name.
* Create `autoexec.cfg` with a single line in it: `exec my`.
* Optional: create an empty `q3config.cfg` and mark it as read-only.
    * Pro: less duplicated work for the engine on load. It's dumb to set every variable twice.
    * Pro: if your config is manually created and doesn't set every cvar, it means `q3config.cfg`'s content actually matters and your "real" config is split among both files.
    * Con: you don't have the latest changes saved anymore. If you ever forget to `writeconfig`, you can no longer salvage the changes from `q3config.cfg`.

In practical terms:
* You can *never* accidentally change or mess up your config by executing another config.
* You update your config by either using `writeconfig my` or manually editing `my.cfg`.
* Selecting your default/start-up config is changing 1 line in `autoexec.cfg`.

## Manually-edited configs

While some players like to create nice looking config files (grouping cvars and binds, adding comment lines with explanations, etc) that they edit manually, we recommend against this practise. Use `writeconfig` instead so that your config file is truly complete and doesn't rely on engine/mod default values (which can change) and/or the content of `q3config.cfg` for any variable you forgot to set in your manually-edited config.

## More details

`default.cfg` should really **only** be found in .pk3 files and provide default binds in case the other 2 configs (`q3config.cfg` and `autoexec.cfg`) don't set up any. Otherwise, launching the game without `q3config.cfg` and `autoexec.cfg`, you wouldn't be able to perform any action in game.

CNQ3 will **not** read `q3config.cfg` and `autoexec.cfg` from baseq3 to avoid config pollution. Sharing configs across mods is still possible:
* In `baseq3`, add `shared.cfg`.
* In each mod folder using the shared config, add `autoexec.cfg` with a single line: `exec shared`.

Configs created by `writeconfig` will contain 3 different commands:
|Name|Instances|Explanation|
|:---|:---|:---|
|unbindall|1|Clears the entire bind list: no action will be bound to any key.|
|bind|1 per bound key|Binds an action (or multiple actions) to a key.|
|seta|1 per variable|Assigns a value to a variable.<br>If the variable doesn't exist, it is created with the archived flag.<br>Only archived variables get written to config files by `writeconfig`.|

## Dealing with vstr scripts

If you have special scripts setting temporary variables for vstr commands (e.g. connecting to servers, toggling values, etc), then here's how to deal with it:
* Put those in a secondary config file, say `my2.cfg`.
* Use the `set` command to create your own temporary cvars, **not** `seta`.
* In `autoexec.cfg`, add `exec my2` after `exec my`.
