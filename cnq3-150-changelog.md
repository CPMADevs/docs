# Changelog for CNQ3 1.50

**Additions**

* on Windows, "/crashreport:yes" and "/crashreport:no" command-line arguments
  they enable the crash handler's quiet mode (no dialog window will be shown)

* the fs_restart command to manually restart the file system

**Changes**

* the client executable will now write to q3config.cfg on exit

* even lower CPU usage when minimized

* during id downloads (cl_allowDownload -1), the time-out is 5 seconds instead of cl_timeout

* the "nextdemo" cvar is now also used when playback stops before reaching the demo's end

* mouse motion is no longer forwarded to the mod when the console is down

* with 2+ monitors, having the console down will always disable input grabbing

* on Windows, a fatal error will move the early console window to the foreground

* com_hunkMegs doesn't have a maximum value anymore
  a value too high would reset it and the engine might fail to load with the default value

**Fixes**

* broken pk3 files could cause crashes (entry names too long) and mess up the FS

* using /demo when running a listen server would load the map and then abort playback

* no longer making local copies of all the system info cvars of the current server
  this avoids undesired local changes to cvars such as sv_pure

* now updating the FS (connected to pure server, pak references) on client disconnects

* the condump command was truncating its file path argument to 64 characters

* "timedemo" playback runs at full speed again

* on Windows, when unfocused, the client would always sleep 5+ ms regardless of com_maxFPS

* conditional FS restarts will always re-order the pak list when connected to a pure server

* lifted the directory scanning restriction that affected pure listen servers

* getting stuck on "Awaiting snapshot..." when the server shuts down during client connection

* demo playback would crash when delta entities/players had an invalid field count

* strcpy calls with overlapping buffers (undefined behavior), which were responsible for the
  "chars.h not found" / "couldn't load any skills" issue preventing bots from being added

* on Linux, the 'Z' and '9' keys could not be bound

* cl_allowDownload 1 failing on "connect" (error 10047 on Windows)

* cl_allowDownload 1 was using the current directory instead of fs_basepath

* jitter due to snapshots piling up with the same server timestamp for loopback and LAN clients

* the Windows crash handler would incorrectly consider certain exceptions as fatal