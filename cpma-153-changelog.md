# Changelog for CPMA 1.53 against beta 2

**Additions**

* cg_demoUI <0|1> (default: 1) draws the demo player's user interface

* pressing F2 during demo playback will toggle cg_demoUI

* $(map) will display the map (script) name when used in the SuperHUD or /con_echo

* showing player handicap values in /players and the 1v1/HM scoreboard when not 100

* ch_eventForceColors, ch_eventOwnColor, ch_eventEnemyColor to use own/enemy colors in game events

* new demo player:
	* displaying server pauses in the timeline bar
	* cg_demoSkipPauses <0|1> (default: 0) skips past server pauses during playback

**Changes**

* improved cpmctf1 banners and the grate lamp shader seen on q3wcp14, ospdm5a and q3dm6

* turned the "no nextSnap" CGame drop error into a console warning

* team chat messages from spectators and coaches no longer include locations

**Fixes**

* game type votes could still get overridden on some single-arena maps

* cg_nudge wasn't always disabled when being a spectator

* area mask being cleared causing b0_beta6 artefacts and lowered performance

* maps with too many locations (e.g. cpm14 in TDM) disconnecting players

* switching followed players could break the predicted animation state and first-person gun position

* new demo player:
	* no longer executing commands from the end of the demo when starting playback
	* seeking during a server pause or timeout won't mess up the item timers anymore
	* can no longer follow phantom players in 1v1/HM/DA when PoV switched in a non-MVD
	* no infinite reload loop when the map config CVar contains "vid_restart"
	* increased buffer sizes such that events like kills don't get dropped/ignored
	* fixed game type being detected wrong on multi-arena maps


# Changelog for CPMA 1.53

**Additions**

* showing player handicap values in /players and the 1v1/HM scoreboard when not 100

* new SuperHUD elements: HitMarker and KillMarker (visible when hitting/killing an enemy)

* ch_crosshairHitColor \<string\> (default: "") colorizes the crosshair after hitting an enemy  
	ch_crosshairFragColor \<string\> (default: "") colorizes the crosshair after killing an enemy  
	* set a CPMA color code ([0-9a-z]) to enable or leave empty to disable

* ch_crosshairPulse 2/3 will animate the crosshair's size after hitting/killing an enemy

* new media courtesy of deft
	* higher resolution font sheets, icons, chat balloon and foe sprites
	* new 3D models: armors, health, PUs, ammo, flags, medkit, backpack, shells, grenade
	* new animated medals and animated chat balloon
	* new icons for deaths/suicides and thawing
	* improved shaders for better r_picmip/r_detailtextures/r_depthFade behavior on popular maps

* cg_newModels <0|1> (default: 1) uses deft's better 3D models

* cg_animateChatBalloon <0|1> (default: 1) enables a looping animation of the chat sprites

* ch_animateRewardIcons <0|1|2> (default: 1) sets the reward icon animation mode  
	* 0 - No animation
	* 1 - Play animation once
	* 2 - Play looped animation

* callvote warmupfire, vote_allow_warmupfire (default: 1), server_warmupFire (default: 0)
	* warmup fire lets you shoot weapons during CA round warm-ups
	* shots fired during round warm-ups only inflict damage/knockback to yourself,  
including projectiles shot during round warm-ups that explode during the rounds

* speconly on/off entries in the team menu

* SuperHUD elements can now display CVar values using the same $(CVar) syntax as /con_echo
	* example: PostDecorate { text "Map: $(mapname)"; } will display the map's name

* new simple SuperHUD elements: MapLocation and Score_Difference (own minus enemy)

* new SuperHUD command to specify the space between items: spacing <amount>
	* relevant elements: Console, GameEvents, Chat, WeaponList, WeaponSelection, RewardIcons/Numbers
	* for WeaponList, it defaults to 4 to match 1.52 behavior
	* for WeaponSelection, 0 will default to 12.5% of the dimension to match 1.52 behavior

* new SuperHUD command to specify the fade-in duration: fadein <milliseconds>

* ch_animateScroll <0|1> (default: 0) enables scrolling animations in SuperHUD elements
	* relevant elements: Console, GameEvents, Chat, RewardIcons, RewardNumbers

* new demo player with rewind support that's only supported by compatible engines
	* it has a freecam mode and also allows PoV switching in non-multiview demos
	* a timeline bar is shown at the bottom of the screen for navigation purposes
	* when either moving the cursor or leaving the cursor at the bottom

* /demo_video <name fps> starts writing a .avi file when the new demo player is enabled

* /demo_stopvideo stops writing the .avi file when the new demo player is enabled

* /demo_players prints the list of players during demo playback

* /demo_view selects the view mode when the new demo player is enabled:

	| /demo_view | Action |
	|---|:--|
	| default | uses whatever view was recorded in the demo |
	| freecam | allows the camera to move around freely |
	| toggleFreecam | toggles between freecam and the previous view |
	| prevPlayer | follows the previous player |
	| nextPlayer | follows the next player |
	| player \<ID\> | follows the specified player |

* /demo_seek jumps around in the demo when the new demo player is enabled:

	| /demo_seek | Action |
	|---|:--|
	| relativeTime \<N\> | jumps N seconds ahead (or back if negative) |
	| virtualTime \<TS\> | jumps to the timestamp (timeline bar values) |
	| serverTime \<TS\> | jumps to the timestamp (raw demo values) |
	| nextMajorEvent | jumps to the next major event |
	| prevMajorEvent | jumps to the previous major event |
	| nextMinorEvent | jumps to the next minor event |
	| prevMinorEvent | jumps to the previous minor event |
	| nextFollowedKill | jumps to the next followed kill |
	| prevFollowedKill | jumps to the previous followed kill |

	timestamps are formatted as 'seconds' or 'minutes:seconds'

* /+demo_scrub enables demo scrubbing when the new demo player is enabled
	* moving the mouse to the left/right while the corresponding button is pressed
	* will move the demo back/forward in time

* cg_demoMapOverrides <0|1> (default: 1) allows the new demo player to display another map
	* the override list is loaded from "cfg-maps/demomaps.txt", where each line has the format
	* "DemoName DisplayName", e.g. "cpm3a cpm3b_b1" will display cpm3b_b1 instead of cpm3a

* cg_demoSkipPauses <0|1> (default: 0) instructs the new demo player to skip server pauses

* cg_demoUI <0|1> (default: 1) draws the demo player's user interface

* pressing F1 during demo playback will show the list of usable binds

* pressing F2 during demo playback will toggle cg_demoUI

* ch_locations <0|1> (default: 1) draws the players' locations in the team overlays

* the SuperHUD element GameEvents presents a streamlined feed of gameplay events
	* everyone can see deaths, thaws and flag pickups/drops/captures/returns
	* non-coaching spectators can also see major item pickups and power-up drops
	* demos older than CPMA 1.52 won't show pickups for non-powerup items (RA, MH)
	* demos older than CPMA 1.53 won't show the name of the player who dropped the flag

* ch_eventForceColors <0|1> (default: 0) enables the use of custom team colors for GameEvents
	* ch_eventOwnColor   <string> (default: "7") is the CPMA color code for your own  team
	* ch_eventEnemyColor <string> (default: "2") is the CPMA color code for the enemy team

* ch_chatLines <0 to 64> (default: 3) is the maximum line count Chat can draw

* ch_eventLines <0 to 64> (default: 3) is the maximum line count GameEvents can draw

* /colorlist prints all available color codes in their respective colors

* /hud_list prints all available SuperHUD elements

* new SuperHUD commands to customize highlights:
	* hlcolor \<R G B A\> : highlight color
	* hlsize \<size\> : highlight size as percentage of the smallest dimension
	* hledges [edges] : selects which edges of the outline rectangle are drawn
	* accepted hledges keywords: left, right, top, bottom, L, R, T, B, all, none
	* highlights are enabled in outline mode (all edges drawn) by default
	* to disable, use "hlsize 0" or "hlcolor R G B 0"
	* to enable fill mode, use "hlsize 100" or use any value greater or equal to 50
	* WeaponList has them disabled by default (hlsize is 0)

* new SuperHUD command that can only be used in !DEFAULT: reset
	* it resets all fields to their default values, allowing to clear flags such as monospace

* com_blood 2 draws a less obtrusive blood effect

* cg_weaponConfig_default|none|gauntlet|MG|SG|GL|RL|LG|RG|PG|BFG|hook \<string\> (default: "")
	* these are per-weapon config CVars executed upon weapon switch
	* cg_weaponConfig_default is used when the corresponding weapon config CVar is empty
	* cg_weaponConfig_none  is used when there isn't exactly 1 equipped first-person weapon  
	this happens when in a free-float camera view or in Multi-View

* cg_modeConfig_default|1v1|2v2|CA|CTF|CTFS|DA|FFA|FT|HM|NTF|TDM \<string\> (default: "")
	* these are per-mode config CVars executed when the game mode changes
	* cg_modeConfig_default is used when the corresponding mode config CVar is empty

* cg_teamConfig_free|red|blue|spec \<string\> (default: "")
	* these are per-team config CVars executed when the followed team changes

* cg_mapConfig_default \<string\> (default: "") is the fallback map config used when
	* the corresponding map config CVar (cg_mapConfig_\<mapName\>) is empty or undefined
	* example for cpm3a: cg_mapConfig_cpm3a is used when not empty, cg_mapConfig_default otherwise

* cg_lightningImpact 2 draws sparks in addition to the impact mark

* cg_fragSound \<string\> (default: "0") plays a sound after a kill
	* 0 - No sound
	* 1 - Tonal impact playing the note D
	* 2 - Tonal impact playing the note E
	* 3 - Tonal impact playing the note F#
	* 4 - Tonal impact playing the note G
	* 5 - Cork pop
	* 6 - Cash register
	* 7 - Grappling hook impact
	* leave at 0 or empty for no sound or specify a sound file path including the file extension
	* example: \cg_fragSound "sound/pure3/grapple/dink.wav"

* cg_zoomSensitivity <0.0 to 100.0> (default: 1) scales the mouse sensitivity when zoomed in
	* to match the behavior of older CPMA versions, set it to 1

* /togglezoom alternates between standard and zoomed in FOVs

* /cv is an alias for /callvote for the server console and rcon

* ui_swapMouseButtons <0|1> (default: 0) swaps the left and right mouse buttons in the UI
	* it applies to the main menu, the in-game menu and the multi-view demo playback UI
	* the live multi-view UI still uses +attack instead of a specific key or button

* a team's name will be kept across map changes if it isn't empty and the game type doesn't change

**Changes**

* turned the "no nextSnap" CGame drop error into a console warning

* team chat messages from spectators and coaches no longer include locations

* misc_portal_surface camera wobble uses spawnflags like baseq3 again instead of being forced

* traded a very frequent bug (hearing hit sounds after match/round starts and arena changes)
	* for a rare one (not hearing a hit sound under very unlikely circumstances)
	* this applies to demo playback of any CPMA version as well

* ejected MG/SG shells inherit the yaw angle and account for crouching

* showing the capture medal regardless of cg_oldCTFSounds (before: only when it was 0)

* the maximum cg_fov value is 150 with a compatible engine using depth clamping, 130 otherwise

* DEV gameplay is the same as PM2 except:
	* 1v1: all spawns respect the far half rule, including initial spawns
	* 1v1: spawn selection weighs horizontal and vertical distances differently
	* TDM: simple mega is enabled by default
	* TDM/2v2/FTAG: spawn selection is based on the distances to the closest teammate and enemy

* disabled (most of) the command rate limiting when running a listen server

* the WeaponList SuperHUD element now supports the following functionality:
	* setting horizontal/vertical mode and weapon order with direction L|R|T|B
	* bottom- and center-aligned vertical modes with alignv B and alignv C
	* left- and right-aligned horizontal modes with alignh L and alignh R
	* center-aligned horizontal mode with alignh C (or textalign C like before)
	* background rectangle margins with the margins command
	* customizable highlights with the hlsize, hlcolor and hledges commands
	* WeaponList has its own defaults to maintain backward compatibility:  
	hlsize 0;spacing 4;direction T;alignh L;alignv T

* ch_consoleLines now has a minimum value of 0, which disables display

* the Console SuperHUD element now behaves like GameEvents and supports:
	* drawing from bottom to top with direction B
	* a solid background color with margins per line with bgcolor, fill and margins
	* right and center text alignment with textalign R|C
	* text offsets with textoffset
	* scrolling animations with ch_animateScroll 1

* the mvw_TDM1-4 CVars are now registered and archived for convenience

* movers always gib corpses and players they kill

* improved team overlays with SuperHUD elements Team1-16 (old) and Team1-16_NME (new)
	* Team1-16 display your own/followed team
	* Team1-16_NME display the opposing team when a non-coaching spectator or watching a demo
	* with CPMA 1.53+, armor type and medkit are shown and non-team modes are supported
	* in 1v1/HM/DA, Team1 and Team1_NME are used
	* in FFA, Team1-16 are used, sorted by scores and displaying scores
	* all these SuperHUD elements always have the monospace font option enabled
	* the new system uses less bandwidth while updating every snapshot instead of every second
	* with CPMA 1.53+ demos, the TargetStatus SuperHUD element can be shown for enemies

* cleaner SuperHUD outlines for WeaponSelection and elements that have both "fill" and "color T"

* CQ3 gameplay settings have been reverted to 1.48 status

* now showing the health/armor/pickup stats of bots when running a listen server or playing 1v1/HM

* tweaked the dynamic light properties of first-person weapon muzzle flashes:
	* adjusted positions such that one can still light up walls when pressed against them
	* the radius changes according to a continuous function to flicker less aggressively

* increased the cg_gunOffset value ranges to x:[-10, 40] y:[-120, 130] z:[-60, 90]

* increased the maximum number of entries in the demo menu to 2048

* the visflags SuperHUD command now also accepts keywords to define the bitmask:
	* follow = 1 - visible when following a player "full-screen"
	* free = 2 - visible in free-float camera view
	* coach = 4 - visible in coach view (multi-view for team modes)
	* all = 7 - visible in all scenarios
	* example: "visflags follow coach" is equivalent to "visflags 5"

* removed the "nextmap" vote item because it stopped serving a purpose long ago

* spawn points fade out of existence when too close to the camera

* removed cg_drawRewards and added 2 new SuperHUD elements as replacement:
	* RewardIcons : the medals
	* RewardNumbers : the number of medals
	* to learn how to replicate the look of cg_drawRewards, please refer to hud.cfg in the pk3 file

* the sound for a new reward will get played even if the medal is already displayed

* re-added the "draw3d" SuperHUD command for backward compatibility with old HUDs
	* also, "model" with no argument (which behaves the same) will no longer trigger an engine warning

* flags can spawn a little higher to avoid disappearing (e.g. ojfc-05)

**Fixes**

* switching from one followed player to another could break the predicted player's animation state
	* this would sometimes affect the view weapon's torso attachment point, thereby hiding the weapon

* cg_nudge wasn't always disabled when being a spectator

* maps with too many locations could cause client disconnects on game mode changes
	* too many commands were sent due to too many config string changes
	* example: "map cpm14;wait 100;cv mode TDM"

* when in overtime, switching from "timed overtime" to "sudden death" or "no overtime"  
would only happen once the current overtime period completed

* if you followed a player who left the arena and joined again while you were still in freecam,  
his player model would not render in the world, you'd hear his frag sounds, etc.

* if you followed a player who entered a different arena and you remained in freecam,  
the scoreboard would show data for the arena the player joined instead of the one you're in

* in rare scenarios, timeouts/pauses could reset the game clock as if no time had elapsed at all

* the invisibility skull's position wasn't interpolated wrt crouching/standing state

* drawing certain event entities (e.g. grenade explosions) as spawn points

* forfeiting a 1v1/HM match with /speconly would put you back in the queue at intermission exit

* when in free-float after following a player on a lift, the camera would shake when it was moving

* loading the map config after a game type vote passes except when the config changes the game type

* in CA with startrespawn 0, the start of the first round's countdown could fail to respawn players

* the solid backgrounds of standard SuperHUD elements were not fading out

* limited the grenade bounce sound spam (especially for grenades interacting with movers)

* /speconly 0 when speconly was already disabled would move you into the spectator team

* bots added to the red/blue/free teams during intermission now join once intermission ends

* /addbot without the team now always puts the bot in the arena, not just in 1v1/HM/DA like before

* aspect ratio of icons in weapon lists and team overlays

* team and player names won't get truncated when set and ^ must be followed by a letter or a digit

* cg_thirdPersonAngle and cg_thirdPersonRange were not updated through the demo player key binds

* the LG beam's endpoint position was wrong in the following cases:
	* cg_trueLightning > 0 in third-person view (most visible when changing cg_thirdPersonAngle)
	* cg_trueLightning   0 with the view model (not accounting for crouch/land/... animations)
	* all beams except the one from the view model (not accounting for crouch/land/... animations)

* movers could disappear when playing sounds (e.g. MH lift on 13zone)

* corpses from the dead body queue can no longer block movers nor force them to change direction

* this partial fix affects score/name/player display, warm-up/countdown status, etc.
	* due to ancient bandwidth optimizations, when (re-)connecting to a server or loading a demo,  
	some important data is present twice but with no indication as to which version should be used
	* while there isn't always a way to prove which version is the right one,  
	we now pick differently when the choice is either provably correct or very likely to be

* map rotation didn't validate the user-supplied arena indices before using them

* map rotation could use arena settings from the wrong map when changing map_index manually  
	or loading a map without going through the map rotation system (/map, /cv map, etc.)

* map rotation forced multi-arena maps straight into intermission when no timelimit was specified

* the WeaponList SuperHUD element was allowed to draw in free-float view

* the health and armor icons were not always showing up at the bottom of the duel scoreboard

* resetting all movers on arena restarts (func_door/plat/button/bobbing/train/pendulum/rotating)
	* example: flags disappeared on ojfc-05 when the movers were in a high position at restart time

* the "QUAD Across" location name in locs/cpm4a.cfg would display "UAD Across" with a bad color

* colored location names could get truncated in the team overlays

* bunch of broken multi-view things:
	* com_blood 1 was drawing the blood of the followed players
	* first-person crouching animations were missing when airborne
	* first-person crouching animations weren't smoothed in zoomed view
	* first-person crouching animations were missing in coach view
	* one player's first-person landing animations were replicated to all coach views
	* TargetName and TargetStatus were not set when looking at some players

* handicap doesn't affect damage done to teammates anymore

* negative damage events would quickly lead to client errors (incorrect health/armor in the HUD)
	* they could eventually lead to server errors as well (player deaths)

* the announcer could warn about time and frag limits right after intermission exit

* /callvote now validates the arena index to prevent memory corruption and crashes

* the server info player list would be incomplete when a player name had a trailing caret (^)

* loading maps from the UI on Linux would fail to load the corresponding map scripts (e.g. cpm24)
	* however, map script loads on Linux can still fail in general due to name case mismatches

* after connecting or switching teams, everyone was being shown as unready in the scoreboard

* the bot_minplayers logic would sometimes (try to) kick humans who are spectating bots

* the advance width of lowercase 'j' in the Sansman font was too small, allowing glyphs to overlap

* these votable items will be ignored and warned about when used in custom game mode configs:
	* "map"  would throw a server in an infinite map loading loop
	* "mode" would shut down a server QVM due to a fatal error

* /callvote's help incorrectly filtered votable items based on the arena's game type
	* e.g. "startrespawn" was visible in CTFS (it shouldn't be) but wasn't in DA/CA (it should be)

* flags spawning too low and falling through floors (e.g. q3wcp4)

* game type votes could get overridden on multi-arena maps and some single-arena maps

* in DA/HM, you were unable to respawn after the opponent left mid-match while you were dead

* maps could appear more than once in the UI browser

* the wrong game type could get loaded after starting a practice game through the UI

* starting a FTAG practice game through the UI would yield the "Mode is incomplete" error

* the scoreboard would flash during intermission when requesting new scores

* overtime duration could be callvoted when overtime type wasn't timed

* "/cv ot" would print the default time period instead of the active one
