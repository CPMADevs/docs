# Installation Guide

A few important points before we start:
* CPMA is a free modification (mod) for the commercial game Quake III Arena.
* CNQ3 is a free improved executable (engine) for running Quake III Arena on Windows (Vista and later) and Linux. It is the official engine for CPMA. While not necessary, we highly recommended its use.
* This guide is for Windows Vista and later.
* MacOS X users will want to look at the ioq3 engine to replace the original one.
* Windows XP users should look at the quake3e engine.

## Installing from the DEZ pack (recommended)

* Download and extract [the DEZ pack](https://drive.google.com/uc?id=1z6yJLa4Y1_AcTVK-Gg1GAFxB_Bc3I7br&export=download) (it's a ZIP archive).  
Make absolutely sure **not** to install in `Program Files` or `Program Files (x86)` as it will prevent the game from saving files to disk.
* Locate and copy the 9 missing .pk3 files (pak0 to pak8) from your legally obtained copy of Quake III Arena. The files are `baseq3/pak0.pk3` to `baseq3/pak8.pk3`.  
Where to locate the files:
  * You can fetch the files from a pre-existing local installation.
  * You can buy [Quake III Arena on Steam](http://store.steampowered.com/app/2200/Quake_III_Arena/). See the guide below on how to navigate to the game's directory.
  * You can buy [Quake III Arena on GOG](https://www.gog.com/game/quake_iii_gold).
* You can now run `cnq3-x64.exe` to launch CPMA.  
Use `cnq3-x86.exe` instead of `cnq3-x64.exe` if you have a 32-bit Windows installation.

## Installing on the Steam version

Installation steps:
* Buy and install [Quake III Arena on Steam](http://store.steampowered.com/app/2200/Quake_III_Arena/). You can safely ignore QUAKE III: Team Arena.
* Locate the game's folder.
	* In Steam, go to the `Game Library`, right-click `Quake III Arena` and select `Properties`.
	* In the `LOCAL FILES` tab, click `BROWSE LOCAL FILES...`
* Download and extract the following archives
	|File|Destination|
	|:---|:---|
	|https://cdn.playmorepromode.com/files/cpma-mappack-full.zip|q3/baseq3|
	|https://playmorepromode.com/files/latest/cnq3|q3|
	|https://playmorepromode.com/files/latest/cpma|q3|

	where `q3` is the root folder of your Quake 3 installation.

* You can now run `cnq3-x64.exe` to launch CPMA.  
Use `cnq3-x86.exe` instead of `cnq3-x64.exe` if you have a 32-bit Windows installation.

Extra steps to launch CNQ3 instead of the original client from Steam:
* Delete or move `quake3.exe` and `testapp.exe`.
* Rename `cnq3-x64.exe` (or `cnq3-x86.exe` on 32-bit Windows) to `quake3.exe`.

## Installing from the original Quake 3 CD

* Run the original installer.
	* Make absolutely sure **not** to install in `Program Files` or `Program Files (x86)` as it will prevent the game from saving files to disk.
	* Opt **not** to install Punkbuster when prompted for this and any further installation.
* Download the 1.32 Point Release patch from our [download section](/downloads) and run it.
* Locate the game's folder.
* Download and extract the following archives
	|File|Destination|
	|:---|:---|
	|https://cdn.playmorepromode.com/files/cpma-mappack-full.zip|q3/baseq3|
	|https://playmorepromode.com/files/latest/cnq3|q3|
	|https://playmorepromode.com/files/latest/cpma|q3|

	where `q3` is the root folder of your Quake 3 installation.

* If you don't have a file called `q3key` in your game's `baseq3` directory:
	* Create an empty file called `q3key` in your `baseq3` directory.
	* Edit the file to add your own CD key in it and save it.
	* It will look a bit like this: `0123456789ABCDEF` (16 contiguous characters).
* You can now run `cnq3-x64.exe` to launch CPMA.  
Use `cnq3-x86.exe` instead of `cnq3-x64.exe` if you have a 32-bit Windows installation.

## Alternative sources

Quake 3 can also be bought on [GOG](https://www.gog.com/game/quake_iii_gold) if you don't want to be tied to Steam's DRM platform.

## Configuring the game

Once you have CPMA up and running, you will probably want to configure the game to your liking.  
For this, please consult the following guides:
* [CPMA Client Settings](cpma-client-settings)
* [CNQ3 Client Settings](cnq3-client-settings)