# CNQ3 Commands


## General commands

|Command|Purpose|
|:---|:---|
|cvar_add \<cvar> \<number>|Adds a number to a cvar's value.|
|cvar_mul \<cvar> \<number>|Multiplies a cvar's value by a number.|
|cvar_trim [-f]|Removes all user-created cvars.|
|fs_restart|Restarts the file system.|
|help\|man \<cvar\|cmd>|Prints the help of a cvar or command.|
|searchhelp \<pattern>|Prints all cvars and commands whose help or name contains the pattern.|
|setempty \<cvar>|Set a string cvar to an empty string.|
|unset \<cvar>|Removes a user-created cvar.
|waitms \<milliseconds>|Delay command executions by the specified number of milliseconds.|
|writeconfig [-f]|Writes archived CVars to a config file. Use -f to force writing all cvars.|


## Client commands

|Command|Purpose|
|:---|:---|
|bindkeylist|Prints the list of key names usable by the `bind` and `unbind` commands.|
|dlmap \<map_name>|Starts the download of a map by name if it doesn't exist locally.|
|dlmapf \<map_name>|Starts the download of a map by name.|
|dlpak \<checksum>|Starts the download of a pak by its checksum if it doesn't exist locally.|
|dlstop|Cancel the download in progress, if any.|
|imageinfo \<imagepath>|Prints where the image was loaded from and shaders that reference it.|
|minimize|*Linux* client only. Minimizes the window.|
|minimizekeynames|*Windows* client only. List of names usable with `in_minimize`.|
|monitorlist|Prints all detected monitors and the indices to use with `r_monitor`.|
|registerdemos|Adds Windows file associations for demo files (.dm_68 .dm_67 .dm_66).|
|searchconsole|Begins a new console search.<br>Press ctrl-F when the console is down to bring up the command.<br>Press (shift-)F3 to find the next match going up or down.<br>Just like in cmdlist/cvarlist/etc, * will match any amount of characters.|
|shaderinfo \<shadername> [code]|Prints where the shader was loaded from and optionally the code.|
|shadermixeduse|Prints all images referenced in shaders with conflicting global directives.|
|unregisterdemos|Removes Windows file associations for demo files.|


## Server commands

|Command|Purpose|
|:---|:---|
|uptime|Prints uptimes for the process, the current map and the parent CNQ3 process (if any).|
