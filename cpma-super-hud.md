# SuperHUD

Since the release of CPMA 1.34 we have had extremely configurable HUDs. This document explains some of the Client HUD settings for CPMA.

Note: cvars such as `cg_drawfps`, `cg_drawtimer` plus many others have been removed and their functions moved to the custom hud.

## Creating a HUD .cfg

* ALL hud configs go in Quake3/cpma/hud. CPMA's sample HUDs 1-7 are also in that directory.
* You can select a config using `\ch_file` hud where 'hud' is the file you would like to use (without .cfg). 
* You do NOT exec hud.
* Use `\reloadHUD` to reload your HUD if you have changed it. Must also be used after `ch_file` change.
* The default HUD is in the PK3: if you want to customise it, pull it out of there with the unzip utility of your choice, RENAME it, and put the copy in cpma/hud/. Remember to point ch_file at the new version.
* The default HUD references (nearly) everything that can be drawn. Hud7 has nice examples of decoration use.


## Syntax
The syntax for CPMA's SuperHud is fairly easy to comprehend, and supports both multi-line and single-line arguments.

```
ELEMENT
{
    ELEMENT_COMMAND VALUE
    ELEMENT_COMMAND VALUE
}
```

`ELEMENT { ELEMENT_COMMAND VALUE; ELEMENT_COMMAND VALUE; }`


You can comment using # but // cannot be used.  
Some Element Commands have optional "," separators.

## Elements

Each piece of the HUD is called an ELEMENT. Elements are sorted z-layer-wise alphabetically.

The first element in the hudfile MUST be `!DEFAULT`, and every other element inherits from that default: this gives you an easy way to change fonts/colors/etc for a whole bunch of elements at once. 

`!DEFAULT` can be reused later in the document affecting every following element but it will not overwrite the parameters set by previous `!DEFAULT`(s). It is advisable to place Team1-8 and Chat1-8 at the bottom of your hud config so that `!DEFAULT`s can be used without interfering with other elements.

Element|Description| 
:-------|:----------| 
|PreDecorate and PostDecorate|Elements that can be used to draw separator bars etc. Limit 64 total.|
|AmmoMessage|Low ammo/Out of ammo. The audio feedback "click" can be toggled using `cg_ammoWarning`.|
|AttackerIcon| Enemy who attacked you last.|
|AttackerName| Enemy who attacked you last.|
|Chat1-8 |Chat area. If you want the order of the messages reversed then reverse the Y coordinates.|
|Console| Replacement for id's console. Use with `con_notifyTime -1`.|
|FlagStatus_NME| Status of enemy flag. To get old RED/BLUE, use `color T` and `color E`, plus `bgcolor`. Check hud/old.cfg.|
|FlagStatus_OWN| Status of your flag. To get old RED/BLUE, use `color T` and `color E`, plus `bgcolor`. Check hud/old.cfg.|
|FollowMessage| Following \<Player>.|
|FPS |FPS (Frames Per Second) counter.|
|FragMessage| You fragged \<Player>.|
|GameEvents|Presents a streamlined feed of gameplay events.|
|GameTime| Game Clock.|
|GameType| Game Type - warmup only. Cannot be omitted. To hide place at off-screen x,y coordinates or set alpha 0.|
|HitMarker|Shown after hitting an enemy.|
|ItemPickup| Text string of whatever item you pick up.|
|ItemPickupIcon |Icon of whatever item you pick up.|
|ItemTimers1_Icons to ItemTimers4_Icons|Icons of a group of item timers. To filter by team, use the `itteam` command. To colorize the background by team in CTF/NTF, use `color I`. For more options, see the `ch_timers*` cvars.|
|ItemTimers1_Times to ItemTimers4_Times|Respawn times of a group of item timers. All comments from `ItemTimers1_Icons` apply.|
|KeyDown_Back| Shown when the `+back` key is pressed. See `ch_drawKeys`. |
|KeyDown_Crouch| Shown when the `+movedown` key is pressed. See `ch_drawKeys`.  |
|KeyDown_Forward| Shown when the `+forward` key is pressed. See `ch_drawKeys`. |
|KeyDown_Jump| Shown when the `+moveup` key is presse. See `ch_drawKeys`. |
|KeyDown_Left| Shown when the `+moveleft` key is pressed. See `ch_drawKeys`. |
|KeyDown_Right| Shown when the `+moveright` key is pressed. See `ch_drawKeys`. |
|KeyUp_Back| Shown when the `+back` key is released. See `ch_drawKeys`. |
|KeyUp_Crouch| Shown when the `+movedown` key is released. See `ch_drawKeys`.  |
|KeyUp_Forward| Shown when the `+forward` key is released. See `ch_drawKeys`. |
|KeyUp_Jump| Shown when the `+moveup` key is released. See `ch_drawKeys`. |
|KeyUp_Left| Shown when the `+moveleft` key is released. See `ch_drawKeys`. |
|KeyUp_Right| Shown when the `+moveright` key is released. See `ch_drawKeys`. |
|KillMarker|Shown after killing an enemy.|
|LocalTime| Local time in the "hh:mm" format. |
|MapLocation|Map location name.|
|MultiView|Only visible when following a player "full-screen" with multi-view enabled.|
|Name_NME|Enemy player/team name. Matches `Score_NME`.|
|Name_OWN|Your player/team name. Matches `Score_OWN`.|
|NetGraph |Lagometer.|
|NetGraphPing| Ping.|
|PlayerSpeed| Units Per Second.|
|PowerUp1Icon to PowerUp8Icon| Powerup Icons. PowerUp1_Icon is also used for the O/D indicator in CTFS - and also for FLAG.|
|PowerUp1Time to PowerUp8Time| Powerup Time remaining.|
|RankMessage |Placed 1st with 30 frags.|
|RecordingDemo|Only drawn when currently writing to a demo file. Needs CNQ3 1.51+ or a recent version of Quake3e.|
|Score_Difference|OwnScore - EnemyScore, i.e. by how much you're leading/trailing.|
|Score_Limit| Frag/Cap/Round limit - if set. Cannot be omitted. To hide place at off-screen x,y coordinates or set alpha 0.|
|Score_NME |Enemy score. Cannot be omitted. To hide place at off-screen x,y coordinates or set alpha 0. To get old RED/BLUE use color T and color E, plus bgcolor. Check hud/old.cfg.|
|Score_OWN|Your score. Cannot be omitted. To hide place at off-screen x,y coordinates or set alpha 0. To get old RED/BLUE use color T and color E, plus bgcolor. Check hud/old.cfg.|
|SpecMessage| SPECTATOR, FRAGGED (in CA/CTFS/FT.)|
|StatusBar_ArmorBar| Armor level in bar form. Use `textalign L|C|R` for filling the bar left-to right/bottom-to-top/right-to-left respectively.|
|StatusBar_ArmorCount| Armor level in number form.|
|StatusBar_ArmorIcon| Type of armor - essential in CPM but irrelevant in VQ3.|
|StatusBar_AmmoBar| Ammo level in bar form. Use `textalign L|C|R` for filling the bar left-to right/bottom-to-top/right-to-left respectively.<br>Since 1.52, you can use the `direction` command, which enables top-to-bottom mode as well.|
|StatusBar_AmmoCount| Ammo level in number form.|
|StatusBar_AmmoIcon| Current weapon ammo icon.|
|StatusBar_HealthBar| Health level in bar form. Use `textalign L|C|R` for filling the bar left-to right/bottom-to-top/right-to-left respectively.|
|StatusBar_HealthCount| Health level in number form.|
|StatusBar_HealthIcon| Defaults to Mynx icon but can be any model, image or shader found in pk3s (provided that the pk3 is on the pure server you join).|
|TargetName| Current target's playername - see `cg_drawCrosshairNames`.|
|TargetStatus| Current friendly target's health/armor level - see `cg_drawCrosshairNames`.|
|TeamCount_NME |Players alive on enemy team (CA/CTFS/FT).|
|TeamCount_OWN |Players alive on your team (CA/CTFS/FT).|
|TeamIcon_NME |Defaults to Sarge icon but can be any model, image or shader found in pk3s (provided that the pk3 is on the pure server you join).|
|TeamIcon_OWN| Defaults to Mynx icon but can be any model, image or shader found in pk3s (provided that the pk3 is on the pure server you join).|
|Team1-16|Own team overlay.|
|Team1-16_NME|Enemy team overlay.|
|VoteMessageArena| Multiarena, ra3maps.|
|VoteMessageWorld |Normal votes.|
|WarmupInfo| 10sec countdown, Waiting for players, etc. Cannot be omitted. To hide place at off-screen x,y coordinates or set alpha 0.|
|WeaponList| The WeaponList has to get a little funky to be able to handle all the legacy HUD tricks: W and H are the size of each weapon, not the total. For the horizontal weapon list (`textalign C`), X is the point to center around. Use `fill` to show ammo for weapons you do not have (useful for TDM). WeaponList wraps at the bottom of the screen, so you can sneak a multi-column one in. `textalign R` can be used to mirror each entry (icon to the far right and ammo count right to its left).|
|WeaponSelection|The weapon wheel. Replaces `ch_drawWeaponSelect`.|
|WeaponSelectionName|The name of the selected weapon. Replaces `ch_drawWeaponSelect`.|

## Colors
CPMA HUD uses r g b a, which is red green blue alpha. Each channel can have values from 0-1.

```
Color 1 1 1 1 is white, full alpha
Color 0 0 0 0.5 is black, half transparent
Color 0.25 0.25 1 0.75 is blue, somewhat transparent
```

Note that colors set in SuperHUD do not override colors set elsewhere.  
For example if your `nick` or `name` is "^3hello" it will be yellow regardless.

### Special Colors

`color T` and `color E` for elements to use red/blue according to your current team.  
Useful for flag modes, but not necessesary for tdm/1v1. Set `bgcolor` for these, even for images. See hud/old.cfg.
    
## Element Commands

The commands you can use within an element are:

|Command|Values|Description| 
|:------|:------|:------|
|ALIGNH |`L` \| `C` \| `R`| Horizontal alignment: Left or Center or Right. Default: Left.|
|ALIGNV |`T` \| `C` \| `B`| Vertical alignment: Top or Center or Bottom. Default: Top.|
|ANGLES |p y r [e]|Alters the display of `MODEL`.<br>Order: pitch, yaw, roll, extra.<br>Extra > 0 means panning angle.<br>Extra < 0 means rotation speed.<br>Note that most Q3 models do not work properly with `r_vertexLight` enabled.|
|BGCOLOR |r g b a| Sets the background color for the element.|
|COLOR |r g b a|Sets the foreground color for the element.|
|COLOR |`T`|Modulates the background color by your own team's color.|
|COLOR |`E`|Modulates the background color by the enemy team's color.|
|COLOR |`I`|Modulates the background color by the item timer's team's color.|
|DIRECTION|`L` \| `R` \| `T` \| `B`|Direction: Left-to-right or Right-to-left or Top-to-bottom or Bottom-to-top.|
|DOUBLEBAR|| Makes bars two lines. Obviously only works on `BAR` elements (`StatusBar_ArmorBar` etc). Check out hud7 for examples.<br>Note: Gets a little funky if `RECT` Height is set below 6 as the space between the two bars is 4 pixels and each bar requires 1 pixel (4+1+1=6).|
|DRAW3D||Enables 3D model rendering.|
|FADE |[r g b a]| If the element has a `TIME`, its text will fade from `COLOR` to this linearly over `TIME` milliseconds.|
|FADEDELAY |milliseconds|How long the element will be displayed for before fading if it doesn't update again.|
|FADEIN|milliseconds|Fade-in duration.|
|FILL ||If the element has a background color, this fills the area it occupies with that color. The element must have a width and height.|
|FONT |name| Selects one of a few standard fonts:<br>`CPMA` - Bitstream Vera Bold, which is a high-resolution font best suited to 1024x768 and up - or large font-size on low-resolution<br>`ID` - standard Q3 font - good for small font-size and/or low-resolution<br>`IDBLOCK` - only supports numbers, NOT letters<br>`THREEWAVE` - which is an outlined copy of the id font<br>`SANSMAN` - Digital Graphics Labs' "Enter Sansman".|
|FONTSIZE |xsize [ysize]|The CPMA font is correctly aspect-adjusted already, and generally looks best if you just specify a pointsize. The other two fonts are square, and generally look much better if you specify distinct x and y sizes, with y being 25-50% larger than x. Element texts can be mirrored using negative x y. Note this is an unintended and unsupported "hack", so might be a bit buggy.|
|HLCOLOR|r g b a|Sets highlight color.
|HLEDGES|[edges]|Selects which edges of the outline rectangle are drawn. Accepted keywords: left, right, top, bottom, L, R, T, B, all, none.|
|HLSIZE|size|Sets highlight size as percentage of the smallest dimension.
|IMAGE| name |The name can be:<br>- a path to an image file (extension not needed)<br>- a shader name<br>- a path to a skin file (extension needed) if you specified a `MODEL` before|
|IMAGETC|l t r b|Texture coordinates for the shader specified by `IMAGE`. Order: Left Top Right Bottom.|
|ITTEAM|team|Only keeps item timers of the specified team in CTF and NTF.<br>The team can be:<br>`B` - Blue<br>`R` - Red<br>`N` - Neutral<br>`O` - Own<br>`E` - Enemy<br>This only does team filtering. For sorting and filtering by item types, use the `ch_timers*` cvars.|
|MARGINS|l t r b|Offsets (positive means inward) to apply to the rectangle when drawing an `image` or the non-`fill`ed background when drawing `text`.|
|MODEL|model| Can be any model in the game. Note that most Q3 models do not work properly if `r_vertexlight` is on.|
|MONOSPACE|| By default, all HUD fonts are proportionally spaced, meaning that an "i" takes up less room than an "m". Monospacing forces every character to take up the same amount of space. It's very useful for the team overlay, as it keeps everyone's health and armor in the same columns, but generally looks worse than proportional spacing on everything else, especially chat. It also takes up more room overall.|
|OFFSET |x y z| Offsets `MODEL` along x y z axis.|
|RECT |x y w h |Sets the position and size of the element. Note that text is NOT "clipped" to this rectangle. Elements can be mirrored using negative w h. Note this is an unintended and unsupported "hack", so might be a bit buggy.|
|RESET||Resets all fields to their default values. Can only be used in the !DEFAULT element.|
|SPACING|amount|Specifies the space between items.|
|TEXT| string| Mostly useful for decorations. Requires `RECT`.|
|TEXTALIGN |`L` \| `C` \| `R`|Justify the text either Left, Centered, or Right within `RECT`.|
|TEXTOFFSET|x y|Offsets to apply to the text position after all other computations.|
|TEXTSTYLE |flags|`1` - Drop shadow.|
|TIME |milliseconds|How long the `FADE`-out animation will last if it doesn't update again. Generally used for item pickups, frag messages, chat, etc.|
|VISFLAGS|flags or keywords|`1` or `follow` - Enable when following a player "full-screen".<br>`2` or `free` - Enable in free-float camera view<br>`4` or `coach` - Enable in coach view.<br>`7` or `all` - Enable in all conditions.<br>Example: "visflags `follow coach`" is the same as "visflags `5`"|

## HUD Related CVars

These cvars do not directly influence the SuperHud but are still related to the HUD in some way.
    
CVar|Value|Default|Description| 
:----|:---|:---:|:-------| 
|cg_customLoc | `0` \| `1` | `1` | Use custom locations for a map if possible.<br>Loc files are locs/map.cfg, and must have a "v2" header.<br>Can only replace existing locations.|
|cg_teamChatsOnly | `0` \| `1` | `0` | Controls what text reaches the HUD Chat elements if set, non-team text still shows up in the console area.|
|ch_3waveFont| `0` \| `1` | `1` | Toggle use of the Threewave font (does not affect SuperHud settings).|
|ch_drawKeys|bitmask|`0`|Add up the numbers corresponding to the options you want enabled<br>`1` - When playing<br>`2` - When following another player<br>`4` - During demo playback (won't work with pre-1.50 demos)|
|ch_drawWarmup | `0` \| `1` | `0` |Toggle display of arena settings before a game starts.|
|ch_hiddenElements | element names | `""` | Space-separated list of SuperHUD elements to hide.<br>Custom elements (PreDecorate and PostDecorate) can be specified individually.<br>Example: PreDecorate3 is the third PreDecorate element of the currently loaded HUD config.<br>`hud_hide` will add to this list and `hud_show` will remove from this list.<br>Example: `ch_hiddenElements "Score_NME Score_OWN"` hides scores irrespective of the loaded HUD config. |
|ch_recordMessage | `0` \| `1` | `1` |Gets rid of the sodding "recording blahblah" message. Must be set before demo recording starts.|
|ch_selfOnTeamOverlay | `0` \| `1` | `1` | Toggle your own information showing in the team overlay.|
|ch_shortNames | `0` \| `1` | `0` | Toggle use of `nick` - set a short name to use for team chat/overlay Limited to 5 visible characters, but allows colors Useful to have your nick shown instead of [longclantag] in team chat/overlay<br>Note: `nick` is only used for team chat/overlay. `name` is still used for anything else.|
|ch_timersList|item type list|`"quad bs regen invis haste flight medkit mh ra ya"`|Space-separated list of item types to display in all modes.<br>When not in CTF/NTF, the list's order is the display order.<br>To print the list of tracked items and their short names, use `/itemtimerlist`.|
|ch_timersSync | `0` \| `1` | `0` | Synchronizes all item timer text updates so they all update once per second during the same frame.|
|ch_timersOrderOR | `0` - `3` | `0` | Order within own/red item timers. CTF/NTF only.<br>For elements using `itteam O` or `itteam R` or no filter only.<br>`0` - Flag -> Mid<br>`1` - Mid -> Flag<br>`2` - Custom list (`ch_timersList`)<br>`3` - Custom list inverted (`ch_timersList` inverted)|
|ch_timersOrderEB | `0` - `3` | `1` | Order within enemy/blue item timers. CTF/NTF only.<br>For elements using `itteam E` or `itteam B` or no filter only.<br>`0` - Flag -> Mid<br>`1` - Mid -> Flag<br>`2` - Custom list (`ch_timersList`)<br>`3` - Custom list inverted (`ch_timersList` inverted)|	
|ch_timersOrderTeams | `0` - `3` | `0` | Order of item timer teams. CTF/NTF only.<br>`0` - Own -> Enemy<br>`1` - Enemy -> Own<br>`2` - Red -> Blue<br>`3` - Blue -> Red|
|ch_wstatsTime | seconds | `10` | How long the auto-wstats window stays up at the end of a game.|
|con_notifyTime| seconds \| `-1` | `3` |Uses the CPMA "Console" hud element instead of id's when -1. Doesn't affect the console buffer, just the text in the top left.|
|mvw_DM |x y w h|`"480 48 160 120"`|Picture in Picture window for DM games.|
|mvw_TDM1-4|x y w h|`""`|Controls teamplay Multi View layout. Format is the same as mvw_DM, default no child windows.|
|ch_crosshairHitColor|color code|`""`|Colorizes the crosshair after hitting an enemy.|
|ch_crosshairFragColor|color code|`""`|Colorizes the crosshair after killing an enemy.|
|ch_animateRewardIcons|`0` - `2`|`1`|Sets the reward icon animation mode.<br>`0` - No animation<br>`1` - Play animation once<br>`2` - Play looped animation|
|ch_animateScroll| `0` \| `1` |`0`|Enables scrolling animations in SuperHUD elements.<br>Relevant elements: Console, GameEvents, Chat, RewardIcons, RewardNumbers|
|ch_locations| `0` \| `1` |`1`|Draws the players' locations in the team overlays.|
|ch_eventForceColors| `0` \| `1` |`0`|Enables the use of custom team colors for GameEvents.|
|ch_eventOwnColor|color code|`"7"`|Own team's color.|
|ch_eventEnemyColor|color code|`"2"`|Enemy team's color.|
|ch_consoleLines|`0` - `64`|`3`|Maximum line count Console can draw.|
|ch_chatLines|`0` - `64`|`3`|Maximum line count Chat can draw.|
|ch_eventLines|`0` - `64`|`3`|Maximum line count GameEvents can draw.|


Example for `mvwTDM`:
```
seta mvw_TDM1 "480 65 160 120"
seta mvw_TDM2 "480 190 160 120"
seta mvw_TDM3 "480 315 160 120"
```

### Crosshair Cvars

CVar|Value|Default|Description| 
:----|:---|:---:|:-------| 
|cg_crosshairHealth | `0` \| `1` | `0` | Red crosshair when low on health<br>Must be 0 for `ch_crosshairColor` to work.|
|cg_crosshairSize | string |`"26x32"`| Crosshair x/y scale.<br>With a single number, scales the same on both axis.<br>With 2 numbers separated by 'x', specifies the x and y scales separately.|
|cg_crosshairX | value |`0`| X-axis offset from screen center.|
|cg_crosshairY | value |`0`| Y-axis offset from screen center.|
|cg_drawCrosshair | `0` - `20` | `11` | Switch between different crosshairs.|
|cg_drawCrosshairNames | `0` - `2` | `1` | Toggles TargetName and TargetStatus.<br>`0` - Off<br>`1` - On<br>`2` - On for teammates only.|
|ch_crosshairAlpha | `0.0` - `1.0` | `1.0` | Controls the transparency of the crosshair.|
|ch_crosshairColor | color | `7` | CPMA Color code: 0 to 7, a to z.<br>Important note: `cg_crosshairHealth` overrides this function: it turns it off to use colored crosshairs.|
|ch_crosshairPulse | `0` - `3` | `0` |Controls crosshair size changes.<br>`0` - Disabled<br>`1` - Item pickups<br>`2` - Enemy damage<br>`3` - Enemy kills|
|ch_crosshairText | string | `""` | Use any text sting as crosshair.|

## Known Issues

* Color fails on elements with color codes embedded in their strings (`StatusBar_HealthCount`, `StatusBar_ArmorCount`, `NetGraphPing`, etc).
* Alpha fails on some elements containing images (`StatusBar_AmmoIcon`, `StatusBar_ArmorIcon`, etc).
* Fade fails on some elements containing images without alpha channels (`ItemPickupIcon`, etc).
* If a `!DEFAULT` sets `MONOSPACE`, `FILL` or `DOUBLEBAR`, there is no method to remove it again (so don't do it unless it's at the end of the file).
* `color T` and `color E` act funny when in non-teamplay modes such as 1v1.
* `FILL` plus `bgcolor` when used on elements that disappear without having `TIME` makes `bgcolor` remain indefinitely.
