# Changelog for CPMA 1.52

**Additions**

* SuperHUD element RecordingDemo will only be visible when recording a demo
	* this will only be supported by compatible engines (latest CNQ3 and Quake3e)

* item timers for 1.52+ demos and live view (non-coaching spectators only)
	* ItemTimers* HUD elements | placement, appearance, team filters
	* ch_timers*  CVars        | type filtering, ordering, time sync

* SuperHUD elements for groups of item timers:
    * ItemTimers1_Icons to ItemTimers4_Icons display the icons
	* ItemTimers1_Times to ItemTimers4_Times display the time remaining until spawn

* ch_timersList <string> (default: "quad bs regen invis haste flight medkit mh ra ya")
	* it's a space-separated list of item types to display in all modes
	* when not in CTF/NTF, the list's order is the display order
	* to print the list of tracked items and their short names, use /itemtimerlist

* ch_timersSync <0|1> (default: 0) synchronizes all item timer text updates

* ch_timersOrderOR <0 to 3> (default: 0) is the order within own/red item timers in CTF/NTF
* ch_timersOrderEB <0 to 3> (default: 1) is the order within enemy/blue item timers in CTF/NTF
	* 0 - Flag -> Mid
	* 1 - Mid  -> Flag
    * 2 - Custom list (ch_timersList)
    * 3 - Custom list inverted (ch_timersList inverted)

* ch_timersOrderTeams <0 to 3> (default: 0) is the order of item timer teams in CTF/NTF
	* 0 - Own   -> Enemy
	* 1 - Enemy -> Own
	* 2 - Red   -> Blue
	* 3 - Blue  -> Red

* new SuperHUD commands and command arguments:

| Name                | Description                                                                 |
| --------------------|:---------------------------------------------------------------------------:|
| fadedelay <ms>      | wait time in milliseconds (max. 5000) before the "fade" animation starts    |
| alignh <L|C|R>      | horizontal alignment (Left, Center, Right)                                  |
| alignv <T|C|B>      | vertical alignment (Top, Center, Bottom)                                    |
| direction <L|T|R|B> | direction (Left-to-right, Top-to-bottom, Right-to-left, Bottom-to-top)      |
| textoffset <X Y>    | offset to apply to the text position after all other computations           |
| imagetc <L T R B>   | texture coordinates for the shader specified by "image"                     |
| margins <L T R B>   | offsets (positive means inward) to apply to "rect" when drawing "image" "text" with a non-"fill"ed background  |                          
| itteam <R|B|N|O|E>  | item timer team filter (Red, Blue, Neutral, Own, Enemy)                     |
| color <I>           | changes the background color to match the item timer's team                 |


> L T R B C stand for Left Top Right Bottom Center respectively
> when used in StatusBar_*Bar, "direction" overrides "textalign" and gives access to top-to-bottom mode
> when used with ItemTimers*, "direction" only specifies the axis, the orientation is handled by CVars

* only users with compatible engines will get a full item timers refresh right after /record
	* for the others, the timer data will be invalid until either an arena restart or arena change happens
	* note that /autorecord will always work on all engines

* server_timersNeutralZone <0 to 256> (default: 16)
	* allows some wiggle room in the definition of center map for item timers in CTF/NTF
	* some maps have their mid power-ups slightly off-center (e.g. quad, haste and mega on cpmctf2)

* cfg-maps/itemtimers.txt can be used to tweak some entity positions for item timers
	* some maps have base items at exactly mid-distance between both flags (e.g. bottom YAs on q3wcp14)

* new SuperHUD elements Name_OWN and Name_NME
	* they won't be visible in pre-1.52 demos when in FFA or a 1v1 mode in free-float view

* /con_echo can now output a cvar's value using the $(cvar) syntax (e.g. "con_echo $(s_volume)")

* the players' ready status is now drawn in the 1v1/hm scoreboard during warm-up

* the #V team chat token is the last enemy player you killed

**Changes**

* the #T team chat token is now the last *enemy* player you damaged

* the #D team chat token is now the last *enemy* player who damaged you
	* the AttackerIcon and AttackerName HUD elements now refer to the last *enemy* player who damaged you
    * /messagemode4 will send a private message to the last *enemy* player who damaged you

* ch_drawWarmup 1 no longer draws anything in coach view

* improved the default HUD and added item timers, player names and a demo recording indicator

* removed the "draw3d" SuperHUD command (to load a skin, use "image" after "model")

* com_blood 0 no longer turns off gibs (despite cg_gibs 1) in all modes other than FTAG

* "Add Bots" and "Remove Bots" menu tweaks:
	* fixed all unregistered keys scrolling the list up by 1 line
	* added list scrolling support with home/end/page up/page down

* in free-float view, Score_OWN/Name_OWN is red/1st place and Score_NME/Name_NME is blue/2nd place

* the accepted value range for the fadedelay and time SuperHUD commands is now [0, 5000]

* removed ch_drawWeaponSelect and added 2 new SuperHUD elements as replacement:

| Name                  | Description       
| ----                  | ----              
|	WeaponSelection     | the weapon wheel
|	WeaponSelectionName | the name of the selected weapon

> unlike ch_drawWeaponSelect, the new elements are also active in live and demo follow views
> to learn how to replicate the look of ch_drawWeaponSelect, please refer to the website's guide

* disabled WeaponList and WeaponSelection* in team modes when multi-view is active
	because the weapon list would only consist of the current weapon (it's a MV limitation)

* tweaked demo playback a bit:
	* HOME: sets speed to 1, no longer resumes playback
	* END : toggles play/pause
	* PGUP: multiplies speed by 2 (max.: 32), no longer resumes playback
	* PGDN: divides speed by 2 (min.: 0.125), no longer resumes/pauses playback
	* the demo playback messages are now drawn in coach (multi-)view as well

* spectators can always vote when no human player is in the arena

**Fixes**

* fixes for the power-up list in the HUD when multi-viewing games:
	- the list could sometimes be broken or empty
	- the order now matches the currently loaded SuperHUD config
	- 2v2/TDM: time remaining was ticking down during server pauses
	- 2v2/TDM: the icons were not displayed when the pick-up time wasn't known
	- 2v2/TDM: the countdown was starting at 29 instead of 30

* team chat token data for #C #D #P #T #V now gets cleared during team changes

* first-person gun offsets were wrong when cg_fov was too high or had a fractional value

* disconnecting players could cancel team pauses

* the FollowMessage SuperHUD element was visible for players in the arena during server pauses

* vid_restart could revert the following to former values: scores, warm-up, # of red/blue players alive

* you could get awarded the defense medal by vicinity for protecting a dead or invalid player

* server crash (division by 0) at the end of a CTFS/CA match when printing the stats

* team mode matches could start with an empty team

* callvotes now get validated more strictly to prevent redundancy, mistakes and abuse:
	- on/off items only accept "0" and "1"
	- numeric items only accept numbers in range with no extra characters
	- numeric items only accept numbers different than the current ones
	- the "mode" and "match" items only accept names of modes that exist and are enabled
	- the "item"/"items" item only accepts proper item names with a "+"/"-" prefix
	- the "item"/"items" item only accepts callvotes that would change the current item list
	- the "nextmap" item now gets validated like the "map" item

* in the error menu, pressing any key when the cursor was on the button would transition to the main menu

* in team modes, referee spectators could /coachinvite players to coach the spectator team

* Score_NME was visible even when the score wasn't valid (no (other) player in the arena)

* a very old bug prevented some state changes from being sent to the clients through config strings
	* affected: score blue/1st/red/2nd (sb/sr), # blue/red players alive (pb/pr), warm-up time (tw)

* team and player names now get validated the same way
	* the maximum display length is 16 and leading/trailing whitespace is no longer accepted

* the SuperHUD element StatusBar_AmmoBar never drew the ammo bar with infinite ammo (<= -1)

* the SuperHUD element AmmoMessage could be visible with infinite ammo (< -1) (e.g. mg on wtf24)

* ch_recordMessage 0 wasn't respected when starting demo recording through /record

* incorrect behavior with cg_predictItems 0 (always) and cg_predictItems 1 (sometimes):
	- backpack pickups in NTF would show weapon/ammo names/icons in the SuperHUD and emit weapon/ammo sounds
	- with cg_autoswitch 1 in NTF, pickups of backbacks dropped by players who had
	  a weapon with ammo selected would make you switch to said weapon when you had it
	- cg_predictItems 0 only: power-up pickups didn't show up on the HUD (ItemPickup and ItemPickupIcon)
	pre-1.52 demos will exhibit the same behavior no matter what cg_predictItems setting was used

* the following UI bits were still drawn when cg_draw2D was 0:
	* Multi-View coach | scoreboard, center print, all SuperHUD elements
	* Multi-View zoom  | scoreboard, center print, SuperHUD element WarmupInfo

* connecting players were not always moved to the spectator team after game type changes

* SuperHUD vertical double bars now work correctly in the StatusBar_*Bar elements

* the WeaponList and Console SuperHUD elements can now fade properly

* no longer writing stats and clearing the console when spectating with cg_autoAction flags 1 and 16 set

* ch_playerNames now draws at correct locations regardless of FOV

* when the score limit is greater than 1 in DA, we now display the correct scores
	* they are hidden during playback of pre-1.52 demos since they were incorrect

* map script callvotes were always rejected (e.g. "cv map !cpm17tp")

* player win/loss stats were updated incorrectly after forfeits

* player models were distorted or invisible in the UI when the resolution was too high

* spawn telefrags were still possible due to an incorrect distance test

* the follow specbug (toggling follow/free-float mode with +attack)

* pending votes not timing out in game modes other than 1v1/hm