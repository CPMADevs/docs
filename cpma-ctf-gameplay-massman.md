# Massman CTF Guide

## Disclaimer

This article lists general guidelines, not rules set in stone. Some of the guidelines can or should be overruled depending on the specific status of the game. Unfortunately, the combinations are too numerous to list. You'll have to use a bit of common sense to determine if a rule does not apply in a certain situation.

## Terminology

Name | Description |
:----|:------------|
| FC | Flag Carrier. |
| EFC | Enemy Flag Carrier. |
| PU | Power-Up. |
| Cross-steal | Situation where both flags are being carried by players. |
| Chain cap | getting 2+ flag captures in a row where the pick-up happens pretty much immediately after a capture. |
| Att(acker) | The players designed to recover the enemy flag. |
| Flag d(ef) | The defender sitting on or near the flag. |
| High d(def) | The defender standing high/forward. |
| stack | Your combined health and armor points. |

## Communication

Communication is a very important pillar of team-play. Whenever possible, try to get on Discord voice. It's fun *and* we get to play better Quake.

If you can't get on Discord, can't talk or several teammates aren't on voice, then you can also use say_team chat binds to give your teammates information:
* Have an available item bind. Example `bind ? "say_team ^2Come Get ^7#I"`.
* Have high/mid/low binds. Examples `bind ? "say_team ^3LOW"` and `bind ? "say_team ^2HIGH"`.
* Have a combined flag/weapon drop bind. Example `bind ? "say_team ^7Dropped Stuff for ^3#n; drop flag; drop"`. This combines CTF/NTF flag drop and TDM/2v2 weapon drop in a single bind.

## General advice

* Have a suicide bind. Example `bind ? "say_team ^3Dropped ^7#U; kill"`. You can't /drop PUs, so you have to /kill if you stole Regeneration in front of your flag carrier or Quad in front of your teammate with 200 armor. Note that using /kill incurs a 2.5 second respawn delay penalty.
* If a teammate carries the flag or a power-up, *all* items are his. Do not steal armor/health/PUs in front of your flag or PU carrier.
* Use Quad/Haste for attacking, not defending. The enemy team will expect you to attack with them, so they increase defence in their base, leaving you to waste the PUs in your base without any enemies.
* If you have the flag during cross-steals, your #1 priority is to stay alive. Do not take *any* chances and run away if need be. Don't try to be a hero and take needless fights. Depending on your health/armor/aim/movement, you might want to drop the flag to a teammate.
* If you carry the enemy flag in your own base, avoid standing on the flag stand because it's a highly vulnerable position. Be sneaky, stand in spots that give you multiple exit strategies and be sure to stack up or drop the flag to teammates with a better stack.
* **Prioritize flags** above anything else. If your flag is taken and your teammates are trying to intercept, do not spam "MEGA HERE" 3000 times, just take it.

## Advice for Attackers

* Try to enter the flag room at the same time as your other attack(s) because the enemy flag def having to deal with more than 1 enemy at a time makes his job much, much harder.
* Leave armor/health in your base for your defenders. Attackers steal mid/enemy armors. In NTF, leave ammo and back packs for your defenders since it is their only ammo source, they need it *badly*.
* Use PUs properly. Good defenders know you are coming, so storming in with a PU and no stack will just give the enemy a free PU. Wait for your attack partner, get some stack or drop the PU for someone with good stack.
* Keep an eye on your teammates' health/armor/PU status. Stealing the flag with no stack in front of a teammate with a big stack is bad. Use your drop flag bind if you happen to take the flag by accident.
* Leave weapons dropped in your base that are usually out of reach for the flag d. For instance, on q3wcp14, leave rocket launchers in base. On q3wcp1/cpmctf2, leave railguns. On q3w3/q3wcp15, leave shafts.
* Your primary role is to bring the flag home so don't chase enemies down your own base or middle. If you frag an enemy attacker he will just respawn in front of you and you'll have to deal with him again. By all means shoot at people, but keep moving towards the enemy base, don't stop or turn around.
* Ignore irrelevant enemies when you are carrying the flag and don't worry too much about enemies behind you, especially if they don't have a good weapon. If you frag them they are likely to respawn in front of you, which is much worse.
* Rocket jump more. A rocket jump deals 50 damage at most. That's *nothing* compared to the enemy rails, rockets or shaft that you could have avoided with a rocketjump.
* Intercept the enemy flag carrier. When your flag is stolen consider stopping your attack and instead intercept the EFC depending on your current position.
* If an enemy with a big stack (red armor) and/or major power-up (quad, regen) enters your base, tell your defenders at all times where he is and focus on stopping him.
* In NTF, don't focus on killing enemy defenders too much. They respawn with all weapons and you won't really gain anything from it apart from them being briefly out of position.
* In CTF, try to time and steal the enemy armors and power-ups. Being successful at this will give you a major advantage.

## Advice for Defenders

* Your role is not to frag people but to prevent captures. Frags are just a by-product. Inexperienced defenders will often chase retreating attackers, abandoning their defensive position.
* Flag defender: accept that you simply can't stop all flag carriers and do not chase if they get out of reach. Concentrate on securing your base and leave it to your teammates to intercept. If your base is lost, your flag will just get stolen again after it is returned and you risk the enemy chain-capping on you.
* High defender: secure middle to escort your flag carrier. Good opponents will try to intercept your flag carrier at the middle of the map. This means there will often be no attacks while your team has the flag and you should push forward instead of idling back in base.
* If a flag gets dropped in a position that makes it difficult for attackers to get it, for instance on jumppads, do not return it. Leaving it gives you 30 seconds of easy defending (until the flag returns by itself)
* In fact, you shouldn't return flags unless absolutely forced. The fastest attackers can steal your flag from its spot at high speeds and exit your base even faster. Any uncertainty of your flag's location makes it much more difficult for attackers as they can't attack on autopilot.
* During cross steals, your role is to defend your flag carrier, not the flag spot. It is pointless to guard the empty flag spot as enemies won't go there because they are trying to frag your flag carrier.
* When your teammates are about to get the enemy flag home, return the dropped flag. Do not make your flag carrier stand on an empty flag spot since he will have enemies chasing him and the flag spot is usually very vulnerable. Get close to your flag carrier so you can quickly take the flag if he gets killed.
* If your other defender is protecting an attacker with the flag, then don't stay in base as having 3 defenders won't help: you only have 1 attacker who won't get the flag back and there's isn't enough armor/health for all 3 of you in your own base. Instead, go help the other attacker return the flag.