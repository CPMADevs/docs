# CPMA Commands

Date: 22 Jun 03

This document gives information on all available commands (and their options) for CPMA, for both clients and at the server console.

All commands that require a parameter can be issued without the parameter to gain additional information on the command's current setting even if the command itself is disabled.

The notation used in the commands described herein uses the common Unix semantics in specifying if a parameter is optional or required:
  - Any parameter description enclosed with < ... > is required.
  - Any parameter description enclosed with [ ... ] is optional.

Some of the commands given below also specify "player_id" as an optional or required parameter.  
This "player_id" information is the numeric ID number assigned to a player when connecting to the server.  To find the associated player_id of a player, issue the `\players` command to view a listing of all player-names and their associated player_id.

**Note**: All of the commands listed below are in addition to the commands that are shipped with the default Quake 3 game.

## General Commands (All Modes of Play)

|Command|Description|
:-------|:----------|
|accuracy/topshots [weapon_id]| Shows best player for each weapon.<br>Adding weapon_id shows all player stats for that specific weapon.|
|autorecord|Creates demo with consistant naming scheme.|
|+button5|Use the offhand hook/grapple, if the server allows it.|
|callvote \<cmd> [params]|Calls a vote.<br>The list of available parameters is given later in this document.|
|con_echo \<s1> [s2...]|Prints all arguments in the console.<br>It can output a cvar's value using the $(cvar) syntax.<br>Example: `"/con_echo Volume: ^3$(s_volume)"` will print the current sound volume.|
|currenttime/time|Displays current local time.| 
|maplist|Display available maps for current mode.|
|motd|Views server's current Message of the Day.|
|mvd|Create a multiview demo (all players POVs).|
|osphelp/commands|List of available commands for the particular mode of play on the server.|
|players|Lists all active players and their server ID.<br>This ID is needed for other commands, as shown below.<br>Additional info on client is also given.|
|ref/referee \<password>|Become a referee (admin access).`
|scores|Shows match stats|
|specdefer [N]|Moves you down the spectator queue N slots. If N is not specified, it is set to 1.
|speconly [0\|1]|With no argument, toggles client's involvement in the 1v1 queue.<br>When argument is set, `0` means disable and `1` means enable.<br>If enabled, then clients will never join a 1v1 game.<br>Disabling it will put the client back to the end of the queue to begin playing again.
|stats [player_id]| Shows weapon accuracy stats for a player.<br>If no ID is given, shows stats for the current/followed player.|
|statsall|Shows weapon accuracy stats for all players.|
|statsblue|Shows weapon accuracy stats for the blue team.|
|statsred|Shows weapon accuracy stats for the red team.|
|togglezoom|Alternates between the standard and zoomed-in FOVs.|
|viewall/mv|Multiviews all active players.|
|viewnone|Removes all active views.|
|+wstats|Displays current stats while playing or of the player being followed (if a spectator).<br>In multiview, the stats of the player of the highlighted window will be displayed.|

## Competition Commands (Warmup-Enabled Modes)

|Command|Description|
:-------|:----------|
|ready|Sets player's status to "ready" to start a match.|
|notready/unready|Sets player's status to "not ready" to start a match.|
|pause/timeout/timein|Call or cancel a timeout.|

## Teamplay-Specific Commands

|Command|Description|
:-------|:----------|
|coach|Switches to "coach" specator mode (after being invited to the team), or reinitializes coach's              multi-player view of the coached team.  Coaches can teamchat, issue timeouts, teamready, etc... with or for the team just as if they were a playing member of the team, but they can only spectate the coached team.|
|coachdecline| Declines a "coach" invitation, or resigns coach status for a team if already a coach.|
|coachinvite \<player_id>| Invites a player to "coach" the team.|
|coachkick \<player_id>|   Removes a previously invited coach from a team.|
|drop [item]|Drops available weapon/ammo (TDM/CTF only) or a carried flag (CTF).<br>Type `\drop` by itself to drop the current weapon.|
|team [team]|Chooses a team (`f`=free, `r`=red, `b`=blue, `s`=spectator) or shows current team (if no param is given).|
|lock|Locks a player's team|
|unlock|Unlocks a player's team|
|captains|Shows the names of both team captains.|
|viewred/viewblue|Adds views of the entire red/blue team.<br>As new players join or leave the team, your screen will be updated.|
|say_teamnl \<msg>|Removes the (location) part of a teamchat.|

## Teamplay-Specific Commands (Captains Only)

|Command|Description|
:-------|:----------|
|speclock|Toggles locking team from spectators.|
|specinvite \<player_id>|Invites a player to spectate locked team.|
|teamready/readyteam|Readies all members on the team to start a match.|
|invite/pickplayer \<player_id>|Invites a player to join the captain's team.|
|remove/kickplayer \<player_id>|Removes a player from the captain's team.|
|resign [player_id]|Resigns team captaincy.<br>You can optionally assign captaincy to another teammate by supplying the teammate's ID.|
|teamname \<name>|Sets a team name for the scoreboard etc to use.<br>If no name is given, prints the current one.|

## Duel and HoonyMode Commands

|Command|Description|
:-------|:----------|
|forfeit|Concede defeat.|

## Referee (Admin) Commands

|Command|Description|
:-------|:----------|
|ref \<password>|Become a referee/admin.|
|ref \<vote> \<param>|Works just like callvote settings and info.|
|abort|Aborts a match that is underway.|
|allready|Forces all players to start match.|
|ban \<player_id>|Temporarily bans selected player from server.|
|r_help|List referee commands.|
|ip|Shows players' names and IP addresses.|
|kick \<player_id>|	Kicks selected player from server.|
|lock|Locks both teams from more players joining.|
|lockblue|Locks the blue team.|
|lockred|Locks the red team.|
|promote \<player_id>|Promotes a player to captain status.|
|putblue \<player_id>|Puts specified player on the BLUE team.|
|putred \<player_id>|Puts specified player on the RED team.|
|remove \<player_id>|Removes selected player from team.|
|speclock|Enables spectator locking for both teams.|
|speclockblue|Enables spectator locking for the blue team.|
|speclockred|Enables spectator locking for the red team.|
|unlock|Unlocks both teams to allow more players to join.|
|unlockblue|Unlocks the blue team.|
|unlockred|Unlocks the red team.|
|specunlock|Disables spectator locking for both teams.|
|specunlockblue|Disables spectator locking for the blue team.|
|specunlockred|Disables spectator locking for the red team.|


**NOTE**: All vote options are also available, as any vote a referee calls will ALWAYS pass immediately. This allows the referee to modify all server settings shown in the `callvote` list. The `ref` command can be used to achieve the same effect.

## Voting Commands/Parameters

The following options are available under the "callvote" facility of CPMA. Most options are called with a parameter of `0` or `1` to signal "OFF" or "ON" (0=OFF/DISABLE, 1=ON/ENABLE).

You can get a list of all votable options in the game by typing: `\callvote` by itself with no option name.  
This will list all available server options to vote upon, based upon the current gametype being played on the server.

You can also get the usage, description and current setting info of most server variables by simply typing: `\callvote <option>` without any parameter. For example, you can get the server's current settings to see which items and weapons are enabled or disabled by typing: `\callvote items`.

See the **CPMA Server Settings** page for details of all the votes.

## Demo Playback Commands

|Command|Description|
:-------|:----------|
|+demo_scrub|Enables demo scrubbing when the new demo player is enabled. Moving the mouse to the left/right while the corresponding button is pressed will move the demo back/forward in time.|
|demo_players|Prints the list of players during demo playback.|
|demo_seek \<...>|Jumps around in the demo when the new demo player is enabled.<br>`relativeTime <N>` jumps N seconds ahead (or back if negative)<br>`virtualTime <TS>` jumps to the timestamp (timeline bar values)<br>`serverTime <TS>` jumps to the timestamp (raw demo values<br>`nextMajorEvent` jumps to the next major event<br>`prevMajorEvent` jumps to the previous major event<br>`nextMinorEvent` jumps to the next minor event<br>`prevMinorEvent` jumps to the previous minor event<br>`nextFollowedKill` jumps to the next followed kill<br>`prevFollowedKill` jumps to the previous followed kill<br>Timestamps are formatted as 'seconds' or 'minutes:seconds'.|
|demo_stopvideo|Stops writing the .avi file when the new demo player is enabled.|
|demo_video \<name> \<fps>|Starts writing an .avi file when the new demo player is enabled.|
|demo_view \<...>|Selects the view mode when the new demo player is enabled.<br>`default` uses whatever view was recorded in the demo<br>`freecam` allows the camera to move around freely<br>`toggleFreecam` toggles between freecam and the previous view.<br>`prevPlayer` follows the previous player.<br>`nextPlayer` follows the next player.<br>`player <ID>` follows the specified player.|

## Information Commands

|Command|Description|
:-------|:----------|
|colorlist|Prints all available color codes in their respective colors.|
|hud_list|Prints all available SuperHUD elements.|

## Server Commands

The following command-list describes the commands available on the server console.  
Note, all console commands that shipped with the default Quake3 game are available.

|Command/Variable|Description|
:-------|:----------|
|addbot|If issued with a non-default Quake 3 bot name, CPMA will deterministically load a bot definition with a determined skill level.<br>This selection is based on the bot's numeric representation of its name.<br>Thus, the same bot will be selected when its name is given, every time.|
|filterlist/banlist|Lists all current filters enabled on the server.|
|filterload/playernames/banload|Loads filter/ban list specified in the `filter_file` and `player_file` server variables.|
|ban \<player_id>|Bans a player from the server.|