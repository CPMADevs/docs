# CNQ3 Client Settings

This document explains some of the client settings of CNQ3. It will never be as current as the changelog, so you should always check that with each new release.


## Network Settings

* `cl_maxpackets <30 to 125> (default: 125)`

   The higher value, the more correct info you send to the server and the more you will hit and the less you will warp -- set as high as your connection allows.

* `cl_packetdup <value> (default: 1)`
    
    Set to 0 if your connection is fine. Set higher if you experience much packet loss.
  
* `cl_timenudge <value> (default: 0)`

    This still exists in CPMA, but should always be 0 unless you're so used to "normal" Q3 netcode that you've become dependent on it. All it really does now is mess up the automatic adaptive nudges.
    
* `snaps`

    snaps should NOT be set in your config. CPMA adjusts snaps value according to the server's sv_fps value. If you join a sv_fps 20 server your snaps get set to 20, if you join a sv_fps 30 server your snaps get set to 30, etc.

* `cl_allowDownload <-1|0|1> (default: 1)`

	`-1` : id's old download system.

	`0` : Downloads disabled.

	`1` : CNQ3's download system.

    |  | id | CNQ3 |
    |:---|:---|:---|
    | Speed | Very slow | Fast |
    | Server set-up | `sv_allowDownload 1` | None |
    | Source(s) | The Q3 server itself | CNQ3's map server for exact matches<br>WorldSpawn for inexact matches |
    | Files | Might DL more than 1 file | Always downloads 1 file |
    | Guarantee | File is there | File might be missing |
    | Cases | Connecting to Q3 server | Connecting to Q3 server<br>Starting demo playback<br>Manual start through console commands |

* `net_proxy <ip> (default: "")`

	The proxy server to use when establishing client connections.


## General Settings

* `com_hunkMegs <value> (default: 128)`

    Amount of RAM allocated to the engine. Anything above 256 is plenty.

* `com_maxFPS <value> (default: 125)`

    Framerate cap. Unlike BaseQ3, movement in CPMA is not dependant on FPS.
    You will jump just as high/far with 30fps as with a million fps.
    Set to 125 or 250.
    If you see "connection interrupted" with 250 on high pings, set it back to 125.

* `s_autoMute <0|1|2> (default: 1)`

    Selects when the audio output should be disabled.

    `0` : Never

    `1` : Window is not focused

    `2` : Window is minimized


## Input Settings

* `m_speed <0 to 100> (default: 2)`

    Mouse sensitivity.

* `m_accelStyle <0|1> (default: 0)`

    `0` : Original

    `1` : Quake3e style

* `m_accelOffset <0.001 to 5000> (default: 5)`

    Offset for the power function. For `m_accelStyle 1` only.

* `in_noGrab <0|1> (default: 0)`

    Disables input grabbing.

* `in_mouse <0|1|2> (default: 1)`

    Windows:

    `0` : Disabled

    `1` : Raw input

    `2` : Win32 input

    Linux:

    `0` : Disabled

    `1` : Enabled

* `in_minimize <string> (default: "")` - *Windows* only

    Hotkey to minimize/restore the window.  
    Key names are to be separated by spaces (e.g. "ctrl q").  
    Use `minimizekeynames` to print the list of usable key names.

    *Linux* users can use the `minimize` command instead.


## Console Settings

* `con_scale <0.25 to 10.0> (default: 1.2)`

    Console text scaling factor.

* `con_scaleMode <0|1|2> (default: 0)`

    Console text scaling mode.

    `0` : Text size scales with con_scale but not the resolution.

    `1` : Text size scales with con_scale and the resolution.

    `2` : Text size is always 8x12.

* `con_completionStyle <0|1> (default: 0)`

    Auto-completion style.

    `0` : Legacy style: always prints all results.

    `1` : ET-style: prints once then cycles.

* `con_history <0|1> (default: 1)`

    Saves the command history to a file on exit.

* `con_drawHelp <bitmask> (default: 1)`

    `1` : Enables the help panel

    `2` : Draws the help panel even if the current cvar/cmd has no help text

    `4` : Draws the list of modules the current cvar/cmd belongs to

    `8` : Draws the list of attributes of the current cvar

* `con_col*` - Console color customization cvars

    | CVar | Type | Default | Use |
    |:---|:---|:---|:---|
    | con_colBG | RGBA | 101013F6 | Console and help panel background
    | con_colBorder | RGBA | 4778B2FF | The console and help panel borders
    | con_colArrow | RGBA | 4778B2FF | Console backscroll arrows
    | con_colShadow | RGBA | 000000FF | Text shadows
    | con_colHL | RGBA | 303033FF | Auto-completion highlight<br>See `con_completionStyle 1`
    | con_colText | RGB | E2E2E2 | Normal text (overrides ^7)
    | con_colCVar | RGB | 4778B2 | CVar names
    | con_colCmd | RGB | 4FA7BD | Command names
    | con_colValue | RGB | E5BC39 | CVar values
    | con_colHelp | RGB | ABC1C6 | Help text


## Graphics Settings

* `r_mode <0|1|2> (default: 0)`

    Controls the video mode and resolution selection when `r_fullscreen` is 1.
    
    `0` : No screen video mode change. The resolution is that of the desktop. No upscaling.

    `1` : No screen video mode change. The resolution is `r_width` x `r_height`. The engine does upscaling using `r_blitMode`.

    `2` : Video mode change. The real resolution is `r_width` x `r_height`. Hardware upscaling.
    Only use this mode if you have a CRT and can benfit from higher refresh rates!

* `r_blitMode <0|1|2> (default: 0)`

    Controls the image upscaling mode when `r_fullscreen` is 1 and  `r_mode` is 1.

    `0` : Aspect-ratio preserving upscale (black bars if needed).

    `1` : No scaling, the image is centered.

    `2` : Dumb upscale, the image is stretched to the entire screen (no black bars).

* `r_height <value> (default: 800)`

    Render height in windowed mode or when `r_mode` is 1 or 2 in full-screen mode.

* `r_width <value> (default: 600)`

    Render width in windowed mode or when `r_mode` is 1 or 2 in full-screen mode.

* `r_monitor <0 to monitor count> (default: 0)`

    Windows: The 1-based index of the monitor to display the game on. `0` is the primary monitor.

    Linux: The 0-based index of the monitor to display the game on.

    Use `/monitorlist` to print the list of monitor indices you can use.

* `r_displayRefresh <value> (default: 0)`

    Display refresh rate.
    The engine can't force display refresh rates, it can only ask for one.
    Ultimately, your drivers will decide.

* `r_ext_max_anisotropy <value> (default: 16)`

    Anisotropic filtering quality.

* `r_msaa <value> (default: 0)`

    Anti-aliasing sample count.

* `r_fastSky <0|1> (default: 0)`

    `0` : Normal sky.

    `1` : Skies and portals are pure black.

* `r_gamma <0.5 to 3> (default: 1.2)`

    Gamma correction strength.

* `r_intensity <1.0 to infinity> (default: 1.0)`

    Brightness of non-lightmap map textures.

* `r_brightness <value> (default: 2)`

    Overall brightness.

    If upgrading from another engine or an old CNQ3:  
    `r_brightness` = pow(2, `r_overBrightBits`)

* `r_mapBrightness <value> (default: 2)`

    Brightness of lightmap textures.

    If upgrading from another engine or an old CNQ3:  
    `r_mapBrightness` = pow(2, max(`r_mapOverBrightBits` - `r_overBrightBits`, 0))

* `r_picmip <0 to 16> (default: 1)`

    Texture detail. Lower value equals higher detail.

* `r_swapInterval <0|1> (default: 0)`

    Vertical Synchronisation (V-Sync).
    The engine can't force v-sync to be enabled or disabled, it can only ask for it.
    Ultimately, your drivers will decide.

* `r_textureMode <value> (default: "GL_LINEAR_MIPMAP_LINEAR")`

	Texture filtering mode.

	`GL_NEAREST` : LEGO(R) mode a.k.a. point sampling a.k.a. no filtering.

	Any other value : Proper texture filtering.

* `r_vertexLight <0|1> (default: 0)`

    `1` : Disables lightmap texture blending, but lighting data is still used per-vertex.

* `r_lightmap <0|1> (default: 0)`

    `1` : Only draw the lightmap when a shader has a lightmap stage.

    Mutually exclusive with `r_fullbright`.

* `r_fullbright <0|1> (default: 0)`

    `1` : Don't draw the lightmap when a shader has a lightmap stage.

    Mutually exclusive with `r_lightmap`.

* `r_teleporterFlash <0|1> (default: 1)`

	`0` : Draws a black screen when being teleported

	`1` : Draws bright colors when being teleported

* `r_ignoreShaderSortKey <0|1> (default: 0)`
 
	Ignores the shader sort key of transparent surfaces.  
	Instead, it sorts by depth and original registration order only.

	This is a work-around for broken maps like bones_fkd_b4 (grates drawn in front of simple items).

* `r_mapGreyscale <0.0 to 1.0> (default: 0)`

	Controls the map's desaturation level.

	`0` : Full color (nothing done)

	`1` : Completely monochrome

* `r_mapGreyscaleCTF <0.0 to 1.0> (default: 0)`

	Controls the CTF map surfaces' desaturation level.

	It only applies to the red/blue color-coded surfaces of popular CTF/NTF maps.

	`0` : Full color (nothing done)

	`1` : Completely monochrome

* `r_alphaToCoverageMipBoost <0.0 to 0.5> (default: 0.125)`

	Boosts the alpha value of higher mip levels.

	With alpha-to-coverage enabled, it prevents alpha-tested surfaces from fading (too much) in the distance.


## Audio Settings

* `s_khz <22|44> (default: 22)`

	The sound mixing sampling frequency: 22.05 kHz or 44.1 kHz.