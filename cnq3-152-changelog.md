# Changelog for CNQ3 1.52

**Additions**

* FreeBSD x64 support

* r_backend <GL2|GL3|D3D11> (default: D3D11 on Windows, GL3 otherwise) selects the rendering back-end
  * GL2   - OpenGL 2.0 minimum, OpenGL 3+ features used for r_msaa
  * GL3   - OpenGL 3.2 minimum, OpenGL 4+ features used for faster geometry upload, compute shaders, etc
  * D3D11 - Direct3D 11 hardware feature level 10.1 minimum, feature level 11.0 for compute shaders

   | Feature / r_backend    | GL2    | GL3    | D3D11              |
   |:-----------------------|:-------|:-------|:-------------------|
   | MSAA                   | GL 3.0 | always | always             |
   | MSAA centroid sampling | never  | always | always             |
   | depth fade             | never  | always | always             |
   | alpha to coverage AA   | never  | always | always             |
   | dithering              | never  | always | always             |
   | GPU mip-map generation | never  | GL 4.3 | feature level 11.0 |

* r_depthFade <0|1> (default: 1) enables depth fade rendering  
  * this helps explosions, smoke, blood, etc. not having "sharp edges" when intersecting geometry

* r_alphaToCoverage <0|1> (default: 1) enables alpha to coverage anti-aliasing
  * A2C AA can only work if MSAA is enabled (i.e. r_msaa > 1)
  * it is used to super-sample all fragments of alpha-tested surfaces

* r_dither <0|1> (default: 0) reduces color banding artefacts by adding noise

* r_ditherStrength <0.125 to 8.0> (default: 1.0) controls the amount of noise added by dithering

* r_gpuMipGen <0|1> (default: 1) enables GPU-side mip-map generation
  * this helps improve map load time (less to upload) and image quality (see r_mipGenGamma, r_mipGenFilter)

* r_mipGenGamma <1.0 to 3.0> (default: 1.8) specifies the gamma space in which the textures are stored
  * r_gpuMipGen 1 is required
  * it should be closer to 2.4 but defaults lower to not break the looks of some things (e.g. cpm3a tele)
  * while 1.0 will preserve the original engine's look, higher values will preserve contrast better

* r_mipGenFilter <filter> (default: "L4") specified which filter window is used to generate mips
  * r_gpuMipGen 1 is required
  * the numbers specify the "radius" of the window filters
  * r_mipGenFilter L4  = Lanczos
  * r_mipGenFilter L3  = Lanczos
  * r_mipGenFilter BL  = Bi-linear
  * r_mipGenFilter MN2 = Mitchell-Netravali (B = 1/3, C = 1/3)
  * r_mipGenFilter BH4 = 3-term Blackman-Harris
  * r_mipGenFilter BH3 = 3-term Blackman-Harris
  * r_mipGenFilter BH2 = 3-term Blackman-Harris
  * r_mipGenFilter T2  = Tent (1/3 2/3) -> same as the CPU version (r_gpuMipGen 0)

* r_frameSleep <0|1|2> (default: 2) selects the frame capping mode
  * r_frameSleep 0 = forced OFF (when V-Sync is enabled)
  * r_frameSleep 1 = forced ON  (when V-Sync is disabled)
  * r_frameSleep 2 = automatic  (detects V-Sync status)
  * this only applies to OpenGL rendering back-ends
  * for Direct3D 11, the behavior is always equivalent to "automatic"

* r_gl3_geoStream <0 to 4> (default: 0) sets the geometry upload strategy
  * r_gl3_geoStream 0 = decide automatically
  * r_gl3_geoStream 1 = BufferSubData
  * r_gl3_geoStream 2 = Map Unsychronized
  * r_gl3_geoStream 3 = Map Persistent when possible, else automatic
  * r_gl3_geoStream 4 = AMD pinned memory when available, else automatic
  * this only applies to the GL3 rendering back-end

* r_d3d11_syncOffsets <0|1|2> (default: 2) synchronizes vertex buffer offsets
  * r_d3d11_syncOffsets 0 = split  buffer offsets (more data transferred)
  * r_d3d11_syncOffsets 1 = sync'd buffer offsets (more API calls)
  * r_d3d11_syncOffsets 2 = decide automatically
  * this only applies to the D3D11 rendering back-end

* r_d3d11_presentMode <0|1> (default: 0) sets the Direct3D presentation mode
  * r_d3d11_presentMode 0 = blit + discard
  * r_d3d11_presentMode 1 = flip + discard when available, else blit + discard
  * about the 'flip + discard' presentation mode:
    * Windows 10 is required and tearing must be supported
    * it should help performance by sharing buffer data (less copying)
    * it allows for V-Sync to be disabled with variable refresh rate displays
    * performance issues were observed on some machines, so it's off by default

* r_khr_debug <0|1|2> (default: 2) decides whether to create an OpenGL debug context
  * r_khr_debug 0 = forced OFF
  * r_khr_debug 1 = forced ON
  * r_khr_debug 2 = only in debug builds

* r_rtColorFormat <0|1|2> (default: 0) selects the color format for render targets
  * r_rtColorFormat 0 = R8G8B8A8
  * r_rtColorFormat 1 = R10G10B10A2
  * r_rtColorFormat 2 = R16G16B16A16
  * higher color bit depths help reduce color banding

* r_lightmapGreyscale <0.0 to 1.0> (default: 0) controls the level of color desaturation in the lightmap
  * r_lightmapGreyscale 0 = full color (nothing done)
  * r_lightmapGreyscale 1 = completely monochrome

* r_transpSort <0|1> (default: 0) to select the transparency sorting mode
  * r_transpSort 0 = sort dynamic transparent surfaces
  * r_transpSort 1 = sort all transparent surfaces
  * the drawback of r_transpSort 1 is that gameplay legibility might suffer and results might look
  * quite inconsistent based on the view angles and surface dimensions
  * example: dropped items in the cpm18r acid with cg_simpleItems 1

* global shader directive q3map_cnq3_depthFade <scale> <bias> enables depth fade rendering
  * scale is the depth range over which to fade the currently rendered surface
  * bias  is a value added to the currently rendered surface's depth, positive goes towards the camera
  * whether this is enabled is decided by r_depthFade (must be 1) and r_backend (can't be GL2)
  * depth writes must be disabled in all stages (no shader stage can use the depthWrite directive)
  * the blend mode (blendFunc) must be the same for all stages and must be one of:
  *   (one, one) (src_alpha, one) (one_minus_src_alpha, one) (dst_color, zero) (zero, src_color)
  *   (src_alpha, one_minus_src_alpha) (one, one_minus_src_alpha)

* r_mapBrightness now works on q3map2's external lightmap atlas images

* the renderer can now batch surfaces with different (but sufficiently similar) shaders
  * this new optimization works with the output of "Frozen Sand Particle Studio"

* /modellist /skinlist /imagelist /shaderlist can now filter results with pattern matching

**Changes**

* improved the map download initiation logic and updated the CNQ3 map server addresses

* increased the small zone allocator's size from 512 KB to 4 MB (1 MB in the dedicated executable)

* dynamic lights now use a softer fall-off curve to create softer edges

* reverted an old change to the Escape key handling for cgame
  * this fixes demo playback behavior in CPMA, which enables custom binds after pressing Escape once

* searching for valid sample counts for MSAA in GL2 and GL3 instead of failing right away

* switched away from stb_image for .tga file loading and now using an updated version of the old code
  * this lowers the chances of a fatal error from the zone memory allocator when reading large images

* SSE2 instruction set support is now required

* removed FreeType 2 and the unused R_REGISTERFONT syscalls that were using it

* removed light flares and all related cvars (r_flare*)

* r_textureMode <filter> (default: "best") is now latched and only supports 2 modes:
  * r_textureMode GL_NEAREST    = LEGO(R) mode (nearest-neighbor filtering for a subset of surfaces)
  * r_textureMode anything else = normal  mode (the engine picks the most appropriate filters)

* removed the following CVars: r_measureOverdraw, r_nobind, r_showImages

* r_speeds is no longer cheat-protected
  * it now also reports more precise timings, V-Sync status, GPU time and the selected back-end

* using the roundss SSE4.1 instruction for inlining floor and ceil syscalls when possible

* improved face and grid processing performance with SSE2

* r_lightmap is now latched again
  * the implementation is now more correct and will apply to all surfaces referencing a lightmap
  * it now also supports surfaces referencing q3map2's "external lightmap" atlas images

* r_fullbright is now latched again
  * it now also supports surfaces referencing q3map2's "external lightmap" atlas images
  * r_mapBrightness is now used by r_fullbright for lightmapped surfaces when r_vertexLight is 0

* negative r_swapInterval values will request adaptive V-Sync when using an OpenGL back-end

**Fixes**

* the engine could crash due to render command list overflows

* made more keys bindable on Linux/FreeBSD and /bind now accepts uppercase hex digits

* removing CVars through unset/cvar_trim/cvar_restart could leave data in an invalid state

* r_detailTextures 0 would mess up shaders where "detail" was used in a stage that isn't the last one

* dynamic lights could incorrectly stop applying to moving entities (e.g. cpm25 elevator)

* crash due to lack of memory for the sound system

* incorrect shader state when loading a map with no lighting

* fog-only non-sky shaders were incorrectly handled as sky shaders, which broke fog on many maps

* r_monitor on Linux/FreeBSD is now 0-based and the value doesn't change incorrectly on its own anymore

* when a latched cvar's value is set to its default, the cvar is no longer marked as changed

* broken face normals get fixed up on map load, which helps with dynamic lights (e.g. industrial)

* classes of transparent surfaces (sprites, beams, etc) now get drawn in the right order

* dynamic lights can now affect transparent surfaces and double-sided surfaces on both sides

* dynamic lights no longer "reveal" the diffuse texture when r_lightmap is 1

* incorrect image flags for various things (clamp mode, mip generation, anisotropic filtering, ...)

**Known issues**

* OBS captures go blank after doing a video restart with the D3D11 back-end
  * Explanation: device/swap chain re-creation isn't handled by OBS' capture plug-in
  * User work-around: use the GL3 back-end
  * Development status: won't fix

* Once the GL2 back-end is used, performance drops when switching back to the GL3 back-end
  * Config: nVidia GeForce GTX 1070 - Windows 7 SP1 x64 - drivers 430.64 and 445.87
  * User work-around: don't switch back-ends or upgrade drivers
  * Development status: can't fix (root cause unknown)

* With GL3 and r_gpuMipGen 1, performance collapses big time
  * Config: AMD Radeon HD 6950 - Windows 10 Pro version 10.0.16299 - drivers 15.201.1151.1008
  * Explanation: confirmed: whenever glTexStorage2D is called (even when using that function is the only change)
  * User work-around: use D3D11 or upgrade drivers
  * Development status: won't fix

* With GL3 and r_gpuMipGen 1, the GPU-generated mips are corrupted
  * Config: AMD Radeon R7 360 - Windows 7 SP1 x64 - drivers 14.502.0.0
  * Explanation: not confirmed: broken barrier implementation?
  * User work-around: use D3D11 or upgrade drivers
  * Development status: can't fix (root cause unknown)

* With GL3, depth faded surfaces break anti-aliasing
  * Explanation: can't fetch from the right sample index because render buffers are used as FBO attachments instead of textures
  * User work-around: use D3D11
  * Development status: will get fixed

* Strong gamma correction breaks anti-aliasing
  * Explanation: MSAA resolve happens before the post-process pass
  * Development status: will get fixed for D3D11 and GL3

* D3D11 full-screen toggles with the 32-bit client freeze myT's PC :-(
  * Config: nVidia GeForce GTX 1070 - Windows 7 SP1 x64 - drivers 445.87
  * User work-around: use the 64-bit client
  * Development status: can't fix (root cause unknown)