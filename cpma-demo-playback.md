# Key Operations

Keystrokes and mouse clicks no longer abort demos. If you have binds that you wish to use, press ESC once. However there are a standard set of keys for most common demo functions:


 ## Standard Demos
 
|Key|Function|
:--|:---|
|F1 | Displays the help screen.|
|F2 | Toggles the demo UI.|
|F11 | Takes a screenshot.|
|TAB | Brings up the scoreboard.|
|PGUP / PGDN | Multiplies/divides playback speed by 2 (min=0.125, max=32).|
|HOME | Sets normal playback speed (1).|
|END | Toggles play/pause.|
|KP_5 toggles | Third-person rendering.|
|KP_UPARROW / KP_DNARROW| Moves camera to / from player.|
|KP_LEFT / KP_RIGHT | Moves camera around player.|
|KP_HOME | Resets camera distance and angle.|
|'.' | Jumps 30s ahead. |


## MultiView Demos Only

|Key|Function|
:--|:---|
|SPACE/MOUSE2 | Switches to next player.|
|ALT | Switches to previous player.|
|MOUSE1 | In a coachview, zooms in to that player.|
|MOUSE1 | In a main view, switches to coachview.|
|MOUSE1 | On a subview, switches to that player.|
|B| Blue Flag, i.e. *red* FC (CTF duh!).|
|R| Red Flag, i.e. *blue* FC.|
|F| Either FC.|
|I| Invis player.|
|Q| Quad player.|
|S| Battle suit player.|
|P| Any player with a real PU (i.e. not Regen/Flight/Speed).|
|V/MOUSE3| Toggles viewcam.|

for F and P, if there's more than one PU/flag in play
each keypress will cycle to the next one

Note that `cg_followpowerup` and team overlay work during MVD playback.


## Demo Browser

CPMA supports directory trees of demos.  Hitting ESC in a demo menu tree pops you up one level rather than straight back to the main menu.

"Back" still jumps all the way out just in case you like it. Once you find the demo you desire, you can hit ENTER to launch it or select Play.
