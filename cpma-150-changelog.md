# Changelog for CPMA 1.50

**Additions**

* new SuperHUD element: MultiView. the element is only visible when following a player full-screen with multi-view enabled.
* new SuperHUD command: visflags <bitmask>
    * 1 - enable when following a player "full-screen"
    * 2 - enable in free-float camera view
    * 4 - enable in coach view

* forfeit support and persistent scoreboards for duel (1v1 and HM). a duel game is forfeited if a player voluntarily leaves the match early (/team s, /disconnect).
* the scoreboard's spectator list now indicates who is speconly (S) and referee (R).
* new default SuperHUD config and new key textures (thanks df).
* hud_takeScreenshots takes a screenshot for each SuperHUD config at the root of the "hud" directory. screenshots will be .jpg files if cg_useScreenShotJPEG is 1 and .tga otherwise with compatible engines, the console will be hidden automatically.
* cg_drawBrightWeapons <bitmask> (default: 0) for fullbright weapon models:

    * 1 - first-person, your own gun.
    * 2 - first-person, carried by teammates.
    * 4 - first-person, carried by enemies.
    * 8 - weapons lying on the map.
    * 16 - free-float camera, carried by players.

* fullbright player models (e.g. "keel/fb" for fullbright keel).
* ui_sensitivity <0.0 to 100.0> (default: 1) is the mouse sensitivity in the UI.
* cg_mvSensitivity <0.0 to 100.0> (default: 1) is the mouse sensitivity in the live multi-view UI. this is only used with engines that don't have the cgame input forwarding extension 	during demo playback, ui_sensitivity is used instead.
* ch_consoleLines <1 to 64> (default: 3) is the max. line count shown by the 'Console' element
* server_useMapModes <0|1> (default: 1) enables game mode overrides on map loads, cfg-maps/mapmodes.txt is used to populate the "Create Server" UI and select suitable game modes on server map loads. for all the details, see the top of the file itself.
* with compatible engines, drop and disconnect errors will be displayed in the UI.
* notify compatible engines of match starts you've missed (see "/help cl_matchAlerts" in CNQ3).
* with compatible engines, end-game screenshots will hide the console.
* support for the new help and cvar range extensions from compatible engines.
* drawing movement keys for yourself and other players:

    * new SuperHUD elements: KeyDown/Up_<key>, where key is one of Back/Forward/Jump/Crouch/Left/Right example: the SuperHUD element KeyDown_Jump is only displayed when the jump key is pressed.

* ch_drawKeys <bitmask> (default: 0) to enable/disable the new KeyDown/Up_<key> SuperHUD elements

    * 1 - when you're playing.
    * 2 - when you're following another player.
    * 4 - during demo playback (won't work with pre-1.50 demos).

* new force colors/model options for team games:

* cg_redTeamColors  <CHBLS> (default: "cccab") to specify the color codes of players in the red  team.
* cg_blueTeamColors <CHBLS> (default: "mmmpo") to specify the color codes or players in the blue team.
* CHBLS colors: rail Core, Head, Body, Legs, rail Spiral (like cg_enemyColors).
* cg_forceTeamColors <bitmask> (default: 15) to decide when cg_red/blueTeamColors are used:

    * 1 - enable for your own team when in first person.
    * 2 - enable for the enemy team when in first person.
    * 4 - enable when in free-float camera mode.
    * 8 - only enable for game types with flags.

* cg_teamModel <model> (default: "sarge/pm") to specify the model all your teammates should be using.
* cg_forceTeamModel <0|1> (default: 0) to decide if your teammates use the model specified by cg_teamModel.

* specdefer [N] command to move down the spectator queue N levels (default: 1).
* overtime duration callvote in seconds with "od" or "overtimeduration" (min: 30, max: 300).
* cg_autoAction option 128 to record when joining mid-game or during the countdown this requires option 4 to be set as well.
* colored spawn points rendering in CTF and NTF, initial spawns are not animated.
* cg_drawBrightSpawns <0|1> (default: 0) for making the spawn learning process easier.
* cg_zoomAnimationTime <0 to 500> (default: 150) sets the zoom animation duration in milliseconds.

**Changes**

* DEV gameplay reset to what it was before 1.48.
* Multi-View tweaks and fixes:

    * added auto-completion for /mv (an alias of /viewall).
    * fixed the PiP images not stretching up to the rectangle's edges.
    * enabled MV for DA and disabled MV for FFA.
    * disabled the MV cursor in non team modes.
    * fixed all SuperHUD elements being drawn in coach view.
    * coach view now respects ch_gauntlet.

* renamed OSP's /help to /osphelp.
* improved the map download UI and fixed the incorrect progress percentages.
* improved the SuperHUD warning messages.
* raised the com_maxfps cap to 250.
* "Add Bots" and "Remove Bots" menu tweaks:

    * fixed "Skill" and "Team" not displaying the current value.
    * added selected bot highlighting and mouse wheel support.

* "Game Settings" menu tweaks:

    * fixed the "Game Mode" displayed being wrong for some modes.
    * added display of the current "Overtime Mode" and a new entry for "Overtime Duration".
    * fixed the display of "Game Mode", "Gameplay", "InstaGib", "thrufloors" and "Overtime Mode" in their submenus.

* removed the "Team Orders" menu.
* cg_autoAction screenshots and stats files now use the same file name as the demo.
* votes can neither be called nor pass during intermission.
* follow, follownext and followprev will no longer move you to the spectator team when you're not.
* cg_deferPlayers 1 only applies during matches and pre-match countdowns.
* invalid characters in cg_altLightning don't disable the LG beam anymore.
* invalid s_announcer values will mute it instead of playing placeholder sounds with some engines.
* cg_simpleItems <0|1> (default: 0) is a 0/1 cvar again (i.e. item scaling is now disabled).

**Fixes**

* the "Chat1-8" SuperHUD elements now behave the same as "Console" wrt con_notifytime and cl_noprint.
* the map selection UI would show maps from .arena files even though the .bsp files were missing.
* scores and names can no longer overlap in the 1v1 scoreboard.
* FFA and DA matches would start while the 2nd player was still connecting.
* the 'Console' SuperHUD element would not always draw all the lines in its rectangle.
* cv'ing a map script when the required .bsp is missing will no longer make the next map load use it example: when hub3aeroq3a.bsp was missing, "cv map !cpm22a" followed by "cv map anymap" would mean "anymap" was getting loaded with the cpm22a.map item layout.
* clearing the ready status of all players when a match begins in a round-based game mode.
* cgame wasn't unregistering commands during shutdown.
* squashed the "specbugs" that were introduced thanks to ioq3 servers:

    * 1. Spec queue  bug: a player going spec after you gets to play before you.
    * 2. Spec follow bug: toggling follow/free-float mode with +attack doesn't work.

* choppy animations when the server time is a large value:

    * underwater FOV, 3D elements in the HUD and scoreboards, weapon drift, sine trajectories.

* the game mode under the level shot in the "game server" menu wasn't shown for HM and NTF.
* the credits now respect s_musicVolume.
* the team overlay was sometimes visible for spectators.
* multi-view mouse input sensitivity is now the same as in the UI with compatible engines.
* the player name look-up behavior of follow, cstats, cv remove/kick/referee/unreferee, ... if 2 players had the same name, it would just pick the first one (lowest client number).
* the CTFS match durations in the XML stats files were incorrect.
* sometimes, stats and screenshots were not saved during the intermission.
* /team free/red/blue would make you go spec in non-team modes.
* the server would sometimes not send global events (flag taken/returned/captured, obituaries, etc).
* the UI wasn't centered properly with display aspect ratios less than 4:3.
* demo recording start/stop bugs:

    * demo recording not starting in FFA.
    * demo recording not starting in team games with bots.
    * demo recording not stopping when going back to warm-up.
    * demo recording not stopping when map/restart/mode/gameplay/match votes pass.
    * demo recording and announcer not stopping when a countdown was interrupted (e.g., a duel player got kicked).

* no longer need to use +scores to get the scoreboard data when connecting during an intermission.
* slow-sinking/raising free-floating spectator camera movement.
* the zoom-out animation's start would abruptly change the fov.
* always load the map config after a game type vote passes example: cv map cpm24 followed by cv mode 1v1 was resetting the weapon respawn to 15s.
* voting maps whose names start with a digit is working now example: cv map 13void.
* when a map is callvoted, its name is fixed to use the case of the actual file name Linux servers can then load the map script (case sensitive file systems) and demo files have proper names example: cv map CPM24 will not get incorrectly stuck on weapon respawn 15 on Linux servers anymore.