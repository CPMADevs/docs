# Updating a CPMA dedicated server

Steps to update CPMA on a dedicated server:
- Extract the mod archive to a fresh new folder.
- Copy over your custom stuff. It may include:
  - Map scripts (`cpma/cfg-maps`).
  - Game modes (`cpma/modes`).
  - NTF classes (`cpma/classes`).
  - Config files.
  - Whatever else you have customized or added.
- When done:
  - The mod directory is named "cpma".
  - The mod directory contains *exactly* 1 .pk3: `z-cpma-pak<current_version>.pk3`.

General guidelines for dedicated servers:
- There can only be 1 .pk3 file: `z-cpma-pak<current_version>.pk3`.
- If you modify some existing files (e.g. game mode scripts), copy and rename them to avoid overwrites.

Updating to CPMA 1.50:
- We have a new game mode override option (`server_useMapModes`) that is enabled by default.  
Please check out `cfg-maps/mapmodes.txt` for a full explanation.
- We ship `modes/PUBCTFS.cfg`, so you might want to delete/rename CTFSPUB.cfg if you have it.
