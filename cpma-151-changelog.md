# Changelog for CPMA 1.51

**Additions**

* /forfeit to concede defeat in 1v1 or HM

* ch_hiddenElements <string> (default: "") is a space-separated list of SuperHUD elements to hide
	* this includes custom elements (PreDecorate and PostDecorate) and they can be specified individually
	* example: PreDecorate3 is the third PreDecorate element of the currently loaded HUD config
	* hud_hide will add to this list and hud_show will remove from this list
	* example: ch_hiddenElements "Score_NME Score_OWN" hides scores irrespective of the loaded HUD config

* the LocalTime SuperHUD element displays the local time in the "hh:mm" format

**Changes**

* the Chat1-8 SuperHUD elements behave the same as in 1.48 (no con_notifytime/cl_noprint filtering)

* the PowerUpXX_Icon SuperHUD elements will now display holdable items too and
	the number of PowerUp* SuperHUD element slots was raised from 4 to 8

* /kill is now disabled in the following scenarios:
	CA DA FTAG -> during rounds and countdowns
	CTFS HM    -> during rounds

* StatusBar_ArmorIcon isn't visible with 0 armor (like StatusBar_ArmorCount and StatusBar_ArmorBar)

* in OT pauses, the clock will no longer show the total time elapsed since the first OT started

**Fixes**

* the UI's mouse sensitivity slider now works with CNQ3

* leaving a HM match in progress during the countdown of round 2+ wouldn't forfeit

* the following "items" arguments would fail in a mode script: "-5H", "-25", "-50", "-5A"

* parts of the vote system were stuck on version 1.47.
	* In addition to the 1.48 changes, non-speconly'd spectators can now vote on:
		- referee and unreferee items in all game modes (1.48: all modes except 1v1 and HM)
    	- any item in Duel Arena

* in the server browser, the server selection now survives rescans and sort key changes

* /mvd and /autorecord were creating the same demo name over and over (date/time of last match start)

* cg_autoAction demo recording not happening after /autorecord or /mvd was used

* combined map + mode changes would make clients lose their session data (ref, speconly, etc)

* the forfeiting player can now talk to his opponent during intermission with mutespecs enabled

* the SuperHUD element AmmoMessage now works in multi-view mode

* the scoreboard flash that happened right before respawning after a DA round loss

* "Invalid vote in PassVote: windelay" warning in DA and HM

* the following commands would crash when run at the server's console (or through rcon):
	name<red|blue>, coach<red|blue>, remove

* the duration of the countdown of the first round in CA is now "warmup" seconds

* set the win delays of CA, DA and HM back to what they were in 1.48

* no longer starting demo recording in DA through cg_autoAction

* no longer displaying "all players ready" in FFA and DA

* players spawning at the end of round countdowns instead of the start in CA and DA

* the wrong custom game mode could sometimes be loaded when one's name was the prefix of another
	* (e.g. "PUBCTFS" instead of "PUBCTF")

* fullbright player models now work with NTF

* callvotes for unregistered maps now get immediately rejected

* ch_drawKeys 2 wasn't displaying in zoomed (multi-)view

* HM round countdown missing

* the clock was frozen at 0:00 in sudden death overtimes
