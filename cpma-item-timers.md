# Item timers

## Reference

* The available SuperHUD elements are:
    * `ItemTimers1_Icons` to `ItemTimers4_Icons`  
Icons of a list of item timers.
    * `ItemTimers1_Times` to `ItemTimers4_Times`  
Respawn times of a list of item timers.
* SuperHUD configs should therefore always define item timer elements in matching Icons/Times pairs.
* Each list can be filtered by team in CTF/NTF using the `itteam <R|B|O|E|N>` (Red, Blue, Own, Enemy, Neutral) command.
* If there isn't at least 1 red flag and 1 blue flag, `itteam N` will match all items.
* Backgrounds can be colored by the team of each item timer using the `color I` command.  
The red/blue teams get red/blue colors, other teams (neutral/none) don't draw the background.
* The `direction` command is only used for the axis (L/R -> horizontal, T/B -> vertical).  
Ordering is handled by the `ch_timersOrder*` CVars.
* `ch_timersList` is a list of item type names. It dictates:
    * What types are filtered out. Types not in the list are not displayed.
    * The order in which item timers are displayed based on type.

## Walkthrough

### Super hud

#### Let's start by showing all item timer icons at the top left corner with `ItemTimers1_Icons`.
^^SideBySide


```SuperHUD
*ItemTimers1_Icons
{ 
    rect 0 0 12 16
}
```
 !{caption="cpm3a 1v1"}(/images/guides/cpma-item-timers/it-walk-1.png)

 ^^SideBySideEnd

As you can see, 1 element represents a *list* of item timers.

#### Now we add the respawn times centered on top of the icons by defining `ItemTimers1_Times`.
^^SideBySide

```SuperHUD
ItemTimers1_Icons
{ 
    rect 0 0 12 16 
}

*ItemTimers1_Times
{ 
    rect 0 0 12 16
    textalign C
    font sansman
    fontsize 8 12
}
```

!{caption="cpm3a 1v1"}(/images/guides/cpma-item-timers/it-walk-2.png)

^^SideBySideEnd

#### We add a dark transparent background behind the icons with `bgcolor` and `fill`.
^^SideBySide

```SuperHUD
ItemTimers1_Icons
{ 
    rect 0 0 12 16
    *bgcolor 0.1 0.1 0.1 0.5
    *fill
}

ItemTimers1_Times
{ 
    rect 0 0 12 16
    textalign C
    font sansman
    fontsize 8 12
}
```

!{caption="cpm3a 1v1"}(/images/guides/cpma-item-timers/it-walk-3.png)

^^SideBySideEnd

#### We move the text below the icons with the new `textoffset` command.
^^SideBySide

```SuperHUD
ItemTimers1_Icons
{
    rect 0 0 12 16
    bgcolor 0.1 0.1 0.1 0.5
    fill
}

ItemTimers1_Times
{ 
    rect 0 0 12 16
    *textoffset 0 14
    textalign C
    font sansman
    fontsize 8 12
}
```

!{group="1" groupStart caption="cpm3a 1v1"}(/images/guides/cpma-item-timers/it-walk-4.png)
!{group="1" groupEnd caption="q3wcp9 ctf"}(/images/guides/cpma-item-timers/it-walk-4-ctf.png)

^^SideBySideEnd

* We add a team-colored line below the icons for CTF and NTF:
* We define a background as usual with `bgcolor` and `fill`.
* We modify the background color's RGB or A values with `color I`.
    * When the item timer's team is red/blue, the RGB channels are changed to be red/blue.
    * When the item timer's team is neutral/none, the A channel is set to 0 (i.e. invisible).

^^SideBySide

```SuperHUD
ItemTimers1_Icons
{
    rect 0 0 12 16
    bgcolor 0.1 0.1 0.1 0.5
    fill
}

ItemTimers1_Times
{ 
    rect 0 16 12 1
    color I
    bgcolor 0 0 0 1
    fill
    textoffset 0 6
    textalign C
    font sansman
    fontsize 8 12
}
```

!{group="2" groupStart caption="cpm3a 1v1 same as before"}(/images/guides/cpma-item-timers/it-walk-4.png)
!{group="2" groupEnd caption="q3wcp9 ctf colored lines"}(/images/guides/cpma-item-timers/it-walk-5.png)

^^SideBySideEnd

#### We add margins to space the icons apart using the `margins` command.
^^SideBySide

```SuperHUD
ItemTimers1_Icons
{
    rect 0 0 12.5 16
    bgcolor 0.1 0.1 0.1 0.5
    fill
    *margins 1 1 1 1
}

ItemTimers1_Times
{
    rect 0 16 12.5 1
    color I
    bgcolor 0 0 0 1
    fill
    textoffset 0 6
    textalign C
    font sansman
    fontsize 8 12
}
```

!{caption="q3wcp9 ctf"}(/images/guides/cpma-item-timers/it-walk-6.png)

^^SideBySideEnd

### Console commands

^^Column

`\ch_timersList "mh ra ya rg lg rl"`

!{caption="cpm3a 1v1"}(/images/guides/cpma-item-timers/it-walk-weaps.png)

> Since the map has two YAs, it shows up twice.
> The "??" means unknown respawn time. This is what you'll see when the ProMode mega-health's countdown hasn't started yet.

^^ColumnEnd

^^Column

`\ch_timersList "50hp mh ra"`

!{caption="cpm3a 1v1"}(/images/guides/cpma-item-timers/it-walk-50hp.png)

^^ColumnEnd

^^Column

`\ch_timersOrderTeams 3`

!{caption="q3wcp9 ctf"}(/images/guides/cpma-item-timers/it-walk-blue.png)

>ch_timesOrderTeams 3 means that the blue team is always displayed first.

^^ColumnEnd

In CTF/NTF, the default team order of item timers is: own, neutral, enemy. (`\ch_timersOrderTeams 0`)

To re-order items in the red and blue teams in CTF/NTF, change `ch_timersOrderOR` and `ch_timersOrderEB`

By default, teams are in own, neutral, enemy order.
With `ch_timersOrderTeams`, you can put own/enemy/red/blue first.

By default, own items are in flag-to-mid order and enemy items in mid-to-flag order.
With `ch_timersOrderOR` and `ch_timersOrderEB`, you can put them in flag-to-mid, mid-to-flag, `ch_timersList` or inverted `ch_timersList` order.

When you set red/blue first with `ch_timersOrderTeams`:
* `ch_timersOrderOR` is for red.
* `ch_timersOrderEB` is for blue.

When you set own/enemy first with `ch_timersOrderTeams`:
* `ch_timersOrderOR` is for own.
* `ch_timersOrderEB` is for enemy.

* To see team filtering in action to split up timer groups with `itteam`, see the examples below.

* To make a time list vertical, use either `direction T` or `direction B`.

## Examples

#### Horizontal bar, dark grey background, team-colored line below the icons, text below the line
^^SideBySide

```SuperHUD
ItemTimers1_Icons
{
    rect 638 4 15 14.66
    direction L
    alignh R
    bgcolor 0.07 0.07 0.07 0.5
    fill
    margins 3 1 3 1.66
}

ItemTimers1_Times
{
    rect 638 18 15 0.66
    direction L
    alignh R
    color I
    bgcolor 0 0 0 1
    fill
    textoffset 0 6
    textalign C
    font sansman
    fontsize 10 13.33
}
```

!{}(/images/guides/cpma-item-timers/it-eg-1.png)

^^SideBySideEnd

#### Horizontal bar, background image behind each icon, team-colored line below the icons, text below the line
^^SideBySide


```SuperHUD
ItemTimers1_Icons
{
    rect 320 50 25 32
    direction L
    alignh C
    color 1 1 1 0.25
    image "gfx/2d/grad_vmid"
    margins 6 8 6 8
}

ItemTimers1_Times
{
    rect 320 82 25  1
    direction L
    alignh C
    color I
    bgcolor 0 0 0 0.75
    fill
    textoffset 0 8
    font sansman
    fontsize 12 16
    textalign C
}
```

!{}(/images/guides/cpma-item-timers/it-eg-3.png)

^^SideBySideEnd

#### Horizontal bar, dark grey background, team-colored line below the icons, text on top of the icons
^^SideBySide

```SuperHUD
ItemTimers1_Icons
{
    rect 320 386 18 24
    direction L
    alignh C
    bgcolor 0.07 0.07 0.07 0.5
    fill
    margins 1 2 2 1
}

ItemTimers1_Times
{
    rect 320 409 18  1
    direction L
    alignh C
    color I
    bgcolor 0 0 0 1
    fill
    textoffset 0 -11
    textalign C
    font sansman
    fontsize 10 13.33
}
```

!{}(/images/guides/cpma-item-timers/it-eg-4.png)

^^SideBySideEnd

* 3 horizontal bars split by team (own, neutral, enemy)
* Own and enemy teams: text below team-colored bars
* Neutral team: text on top of the icons, background image behind each icon

^^SideBySide


```SuperHUD
ItemTimers1_Icons
{
    rect 245 4 15 14
    margins 2.5 1 2.5 1
    direction L
    alignh R
    bgcolor 0.07 0.07 0.07 0.5
    fill
    itteam O
}

ItemTimers1_Times
{
    rect 245.33 17.33 15 0.66
    direction L
    alignh R
    color I
    bgcolor 0 0 0 1
    fill
    fontsize 8 12
    font sansman
    textalign C
    textoffset 0 6
    itteam O
}

ItemTimers2_Icons
{
    rect 320 30 22 28
    margins 3 4 3 4
    direction L
    alignh C
    image "gfx/2d/recording"
    color 0.07 0.07 0.07 0.3
    itteam N
}

ItemTimers2_Times
{
    rect 320 44 22 0
    direction L
    alignh C
    alignv C
    fontsize 8 12
    font sansman
    textalign C
    itteam N
}

ItemTimers3_Icons
{
    rect 395 4 15 14
    margins 2.5 1 2.5 1
    direction L
    alignh L
    bgcolor 0.07 0.07 0.07 0.5
    fill
    itteam E
}

ItemTimers3_Times
{
    rect 394.66 17.33 15 0.66
    direction L
    alignh L
    color I
    bgcolor 0 0 0 1
    fill
    fontsize 8 12
    font sansman
    textalign C
    textoffset 0 6
    itteam E
}
```

!{}(/images/guides/cpma-item-timers/it-eg-5.png)

^^SideBySideEnd

#### Vertical bar, team-colored backgrounds, text on top of the icons

^^SideBySide

```SuperHUD
ItemTimers1_Icons
{
    rect 550 380 22 28
    direction T
    alignv B
    color I
    bgcolor 0 0 0 0.25
    fill
    margins 2 2 2 2
}

ItemTimers1_Times
{
    rect 550 380 22 28
    direction T
    alignv B
    textalign C
    font sansman
    fontsize 12 16
}
```

!{}(/images/guides/cpma-item-timers/it-eg-2.png)

^^SideBySideEnd