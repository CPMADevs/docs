# Changelog for CNQ3 1.53 against beta 2

**Additions**

* /registerdemos and /unregisterdemos for Windows file associations of .dm_68 .dm_67 .dm_66 files

* on Windows, /demo can now play demo files that have an absolute path (e.g. /demo C:\test.dm_68)

* r_teleporterFlash <0|1> (default: 1) draws bright colors when being teleported

**Fixes**

* new demo player:
	* no longer executing commands from the end of the demo when starting playback
	* seeking during a server pause or timeout won't mess up the item timers anymore
	* video restarts causing fatal errors when demo file names contain spaces

* long demo file names would cause video recording to output files with no extension or outright fail

* mirror/portal clipping in the OpenGL 2 back-end (not working on AMD cards)

* the CPU mip-map generator was shifting the image towards the top-right

# Changelog for CNQ3 1.53

**Additions**

* /registerdemos   adds Windows file associations for demo files (.dm_68 .dm_67 .dm_66)
  * /unregisterdemos removes Windows file associations for demo files
  * in addition, /demo can now play demo files that have an absolute path (e.g. /demo C:\test.dm_68)

* r_teleporterFlash <0|1> (default: 1) draws bright colors when being teleported (use 0 for black)

* s_khz <22|44> (default: 22) is the sound mixing sampling frequency: 22.05 kHz or 44.1 kHz

* /writeconfig now has a "-f" option to force writing all CVars (whether archived or not)

* /imageinfo \<imagepath\> prints where the image was loaded from and shaders that reference it

* /shadermixeduse prints all images referenced in shaders with conflicting global directives

* cl_demoPlayer <0|1> (default: 1) selects the demo playback system to use
  * cl_demoPlayer 0 = always uses the original demo player
  * cl_demoPlayer 1 = uses the new demo player with time rewind support when the mod supports it

* cl_escapeAbortsDemo <0|1> (default: 1) decides whether the escape key can abort demo playback
  * reminder: you can always use /disconnect to stop demo playback

* r_depthClamp <0|1> (default: 0) disables vertex clipping against the near and far clip planes
  * enabling this feature will raise the maximum allowed FoV value of CPMA 1.53
  * it turns the view frustum into a pyramid and prevents any vertex between the near clip plane  
  and the camera's position from getting clipped (but will still clip anything behind the camera),  
  which prevents solid objects from being seen through when too close to the camera
  * because depth values are clamped, improper depth ordering can potentially occur

* /shaderinfo \<shadername\> [code] prints where the shader was loaded from and optionally the code

* console mark mode (selection mode) can be toggled with Ctrl-M
  * use arrow keys, home/end or page up/down to move the cursor or extend the area when pressing shift
  * Ctrl-C/enter copies the selected text to the clipboard without color codes and exits mark mode

* /searchconsole begins a new console search
  * press ctrl-F when the console is down to bring up the command
  * press (shift-)F3 to find the next match going up or down
  * just like in cmdlist/cvarlist/etc, * will match any amount of characters

* r_ignoreShaderSortKey <0|1> (default: 0) ignores the shader sort key of transparent surfaces
  * instead, it sorts by depth and original registration order only
  * this is a work-around for broken maps like bones_fkd_b4 (grates drawn in front of simple items)

* r_mapGreyscale <0.0 to 1.0> (default: 0) controls the map's desaturation level
  * r_mapGreyscale 0 = full color (nothing done)
  * r_mapGreyscale 1 = completely monochrome

* r_mapGreyscaleCTF <0.0 to 1.0> (default: 0) controls the CTF map surfaces' desaturation level
  * r_mapGreyscaleCTF 0 = full color (nothing done)
  * r_mapGreyscaleCTF 1 = completely monochrome
  * it applies to the red/blue color-coded surfaces of popular CTF/NTF maps

* net_proxy \<ip\> (default: "") is the proxy server to use when establishing client connections

* /waitms \<milliseconds\> to delay command executions by the specified number of milliseconds

* r_alphaToCoverageMipBoost <0.0 to 0.5> (default: 0.125) boosts the alpha value of higher mip levels
  * with A2C enabled, it prevents alpha-tested surfaces from fading (too much) in the distance

**Changes**

* with hybrid core CPUs (e.g. Intel gen 12+) on Windows, the main thread will only run on the P-cores

* the client can now read .wav files from the local file system when connected to a pure server
  * this allows e.g. cg_fragSound to load sounds from the local file system instead of .pk3 files

* CVar sets will use all arguments instead of only the first one
  * example: pressing n with `bind n "name x y z"` will rename to "x y z" instead of "x"

* invertedpenguin's RA blood pool has a new look to help better see the RA against its background

* forcing the nopicmip flag on 2 new texture folders: "textures/npmenv" and "textures/npmpads"

* r_vertexLight 1 no longer collapses shaders but replaces lightmaps with vertex colors
  * for transparent shaders, lightmaps are still used because vertex colors are broken on some maps

* bots can no longer get timed out (it happened sv_timeout seconds after bot_pause was enabled)

* r_gpuMipGen 0 now respects r_mipGenFilter but not r_mipGenGamma (gamma 2 is used)
  * the new code will slow down map loads in general but produces higher quality results
  * r_mipGenFilter BL is a special case without gamma correction that's much faster than before

* image load/save errors now print warnings instead of triggering drop errors

* the maximum texture resolution of 2048 is now enforced in all image loads

* updated icon courtesy of deft

* r_showtris/r_shownormals \<bitmask\> (default: 0) draws wireframe triangles/vertex normals
  * 1 - enables the feature
  * 2 - only draws for visible triangles
  * 4 - draws for backfacing triangles
  * 8 - uses the original vertex color
  * 16 - uses the original vertex alpha as vertex color

* r_lightmap 1 now handles most alpha-tested surfaces

* com_soundMegs now defaults to 16

* the camera's near clip plane distance is now 1 instead of 4

* cl_aviFrameRate now defaults to 60

* the Windows audio output now has a sampling rate of 44.1 kHz
  * with this, /video won't have issues with cl_aviFrameRate 60 anymore

* with r_backend GL3, depth fade with MSAA now requires GLSL 4.00 at a minimum
  * depth access is now correctly multi-sampled (less aliasing) and performance has improved as well

* with r_backend GL3, alpha to coverage now requires GLSL 4.00 at a minimum

**Fixes**

* the renderer could crash on some maps without lightmaps (e.g. b0_beta5)

* with r_backend GL2, the extra clip plane for mirror/portal surfaces was not being used
  * this quick fix is likely to fail for some drivers and vendors due to its use of deprecated features  
  * based on a few tests: OK on NVIDIA and Intel, not working on AMD

* long demo file names would cause video recording to output files with no extension or outright fail

* invalid skybox texture mip-mapping and filtering settings (e.g. cpm25 skybox seams)

* in render batches with multiple shaders where the second or later surface had a shader using
  * "rgbGen lightingDiffuse", the colors would be wrong and crashes could occur

* the reported MSAA sample counts for the GL2 and GL3 back-ends could be wrong

* registration of a read-only CVar would keep the existing value

* the Direct3D projection transform didn't perfectly match the OpenGL version

* /video was writing incorrect audio stream lengths to the .avi file headers

* /systeminfo /serverinfo /clientinfo /dumpuser would crash when tokens were too long

* with r_lightmap 1 and r_dynamiclight 1, lighting transparent surfaces could crash

* delayed shader loads could lead to incorrect rendering and crashes (mostly with dynamic lights)

* the nextdemo variable wasn't used to start the playback of a new demo when:
  * a drop error happened during demo playback
  * the engine failed to open the current demo file

* the Linux/FreeBSD client could crash during start-up

* with r_backend D3D11, the alpha channel's blend factors now match the original Q3 behavior
  * this affects shaders that use the following blendFunc modes: GL_DST_ALPHA, GL_ONE_MINUS_DST_ALPHA
  * examples: jumppads on cpm3a and cpmctf3, jumppad trims on q3wcp14, shiny walls on q3wcp9

* batched surfaces with "tcMod turb" would generate incorrect texture coordinates

* throwing a fatal error when com_soundMegs is too low to load a map instead of freezing

* /video produced .avi files that started with silence when they were not first in the sequence

* with r_backend D3D11, some device reset scenarios caused fatal errors instead of video restarts

* CVar slots that have been freed (e.g. through unset/cvar_trim/cvar_reset) can now be re-used

* with alpha to coverage enabled, high r_picmip values should no longer cause  
  alpha-tested surfaces to become transparent in the distance (e.g. q3wcp14 corridor floors)

* with r_backend GL3, caching of shader uniforms could break with fog-only shaders

* with r_backend D3D11, partial clears incorrectly affected entire render targets
  * example: black views in CPMA multi-view with r_fastsky 1
