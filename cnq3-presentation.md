# The *Challenge Quake 3* (CNQ3) Engine

Here are lists of the major improvements over the original *Quake III Arena* engine.

## General
* 64-bit support.
* Improved auto-completion.
* Detailed crash reports, including VM stack traces.
* Built-in help system and help text (`/help` and `/searchhelp`).
* Many bug fixes.
* It is the official *CPMA* engine (not required, but recommended).

## Client
* Better-looking dynamic lights (`r_dynamiclight 1`).
* A fast download system that gets the right files (`cl_allowDownload 1`).
* Gamma correction is always available and will never ever change your screen settings (`r_gamma`).
* Faster map loads and reduced CPU usage.
* Monitor selection (`r_monitor`).
* Help panel drawn below the console (`con_drawHelp 1`).
* Windows: raw mouse input support (`in_mouse 1`).
* Linux: new code for window, input, video and audio.

## Dedicated server
* Linux: built-in process restart. Process restarts can happen after a certain amount of time has passed (and no player is connected) or after a crash.

For more details on the client settings, head here: [CNQ3 Client Settings](cnq3-client-settings).  
For the list of new commands, head here: [CNQ3 Commands](cnq3-commands).