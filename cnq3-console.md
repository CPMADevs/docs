# CNQ3 console guide

This guide is not intended to teach the basics of the Quake 3 console. Instead, it shares tips and details that will allow you to use it more effectively.

## Quick recap

- The console is a command-line interface that lets you type in commands, change and see variable values.
- To activate or deactivate the console, you press the tilde key (~).
- The bottommost line always contains the current user input buffer.
- Once you're done typing your message or command, you press enter for the system to execute it.
- The caret is the blinking cursor that is either an underscore (\_) or a full rectangle block (&#9646;).
- All commands start with a leading slash (/) or backslash (\\).

If you don't type the leading slash or backslash in front of your text and press enter:

1. If in-game, it will send a global chat message.
2. If not in-game, the game will add a leading backslash for you.  
   In other words, it considers everything to be a command.

The two insertion modes are the following:

1. When the caret is a small underscore (\_), a new character gets inserted (added) where the caret is.
2. When the caret is a full rectangle block (&#9646;), a new character replaces the character where the caret is.

Variables accessible through the console are called _console variables_ or _cvars_ for short.

## Auto-completion

Running auto-completion (by pressing tab) can be used to help speed up several processes:

- To list variables and commands starting with a certain string pattern after you've typed at least one character.  
  Example: type in `r_` and then press tab to get a list of all renderer variables.
- To add the leading backslash.  
  Example: to vote cpm3a, you can type in `cv map cpm3a`, press tab and then enter.  
  The auto-completion recognizes that cv is a command and will add the leading backslash.
- To speed up typing.

## The `help` and `searchhelp` commands

When running auto-completion, CNQ3 will display a short description text next to each result when there is more than one. When a result has full help text available, the letter `h` will be displayed in front of the result's name to let you know.

> `help` \<cvar|cmd>

This will display the full help text for the given cvar or command.
In addition, it will also print the following for cvars:

- The type and/or value range
- Which modules own the cvar

Example: `help cg_autoAction` to see what number corresponds to what feature

> `searchhelp` \<text>

This lists all cvars and commands whose names, descriptions or full help text contain the supplied bit of text.

Example: `help rail`

## CPMA command aliases

Certain commands and vote strings have short aliases to reduce typing:

| Alias      | Command          |
| :--------- | :--------------- |
| cv         | callvote         |
| wr         | weaponrespawn    |
| falldamage | fallingdamage    |
| limit      | scorelimit       |
| ot         | overtime         |
| od         | overtimeduration |
| gp         | gameplay         |
| thaw       | thawtime         |
| tl         | timelimit        |
| ref        | referee          |
| unref      | unreferee        |
| mute       | mutespecs        |

## CPMA connection info

Use the `cstats` command to check your ping range for both client-to-server and server-to-client. With no argument, it gives you information for yourself. The command can take a player ID if you want to see the stats of someone else.

## Search

There are two very useful commands for searching variables and commands, called `cvarlist` and `cmdlist`. The best property they have is the _ matching syntax. The _ will try to match as many characters as possible that do not match the following pattern.

- `cvarlist crosshair` will print out all variables whose names start with "crosshair".
- `cvarlist *crosshair` will print out all variables whose names contain "crosshair".

The cvarlist command displays a bunch of letters in front of the variables it found. They describe variable types.

## Variable types

Variable types get printed out as a single character when using the `cvarlist` command.

|     | Category     | Description / Remarks                                                           |
| :-: | :----------- | :------------------------------------------------------------------------------ |
|  S  | server info  | Sent in response to front-end requests.                                         |
|  U  | user info    | Sent to server on connect or change.                                            |
|  R  | ROM          | Display only, cannot be set by user at all.                                     |
|  I  | init         | Don't allow change from console at all, but can be set from the command line.   |
|  A  | archive      | Used for system variables, not for player specific configurations.              |
|  L  | latched      | Will only change when C code next does a Cvar_Get() call.                       |
|  C  | cheat        | Can not be changed when sv_cheats is 0.                                         |
|  ?  | user created | Unknown to the game and created by a set command.                               |
|  h  | help         | Full help text is available through `/help` or the help panel (`con_drawHelp`). |

## CPMA - Using `cv` to query values

The `callvote` command can be used to display help and query current CPMA/OSP server settings.

- `cv` will query the help and give you a list of all the things that can be voted.

Suppose that after reading the help, you're interested in the _gameplay_ category.  
`cv gameplay` will display the help of that category, showing you what can be voted.  
It will also tell you what the current gameplay mode is.

## OSP help syntax

Most commands that require at least 1 argument will display some help if you invoke them with no argument.

Example: `callvote`

However, some commands can be run without any argument, like `cstats`, so a different method is needed to display the help. OSP commands have a special argument for querying the help: `?`.

Example: `cstats ?`

## Commands with client numbers

Several commands require a client number as an argument, like `kick`, `remove`, `cstats`, etc.  
Most of those now support passing a player name (without color codes) and will tell you if there's any ambiguity preventing execution.

## Pasting from the clipboard

You can ctrl+c text on the desktop and paste it with ctrl+v into the console.  
This is mostly used for entering IP addresses for the `connect` command.

## Copying to the clipboard

You can ctrl+c text in the console when in mark mode and paste it with ctrl+v into any dekstop application.  
To toggle mark mode, press ctrl+m. To see available key bindings, [click here](#mark-mode-key-mapping).

## The `toggle` command

Toggles a cvar from 0 to 1 and 1 to 0.

Syntax: toggle cvarname

- If the cvar is not 0, it becomes 0.
- If the cvar is 0, it becomes 1.

So, for any boolean values, having the bind `bind space toggle cvarname` means every time you press the space bar, the cvar's value will "flip".

## Finding the last error

After an error occurred, the error message can be found in the cvar `com_lastError`.  
This cvar will not be defined before at least 1 error happened.

Starting with CPMA 1.50, any error that returns you to the main menu will show you a new screen with the error messages so that you don't need to go dig in the console backlog.

## Quotes and command chaining

Command arguments are separated by spaces (``), commands by semi-colons (`;`).  
The problem is that sometimes, you need one of those characters inside an argument.  
The solution to this is enclosing the entire argument in double quotes (`"`).

Command chaining is mostly useful for binding multiple actions to a single key/button.

Example: `bind space "drop;weapnext"` to drop the current weapon and select the next one (otherwise you get the pummel). **Note** that `bind space drop;weapnext` would execute `bind space drop` and then `weapnext`.

## The `reset` command

Resets a cvar to its default value.

Syntax: reset cvarname

The current and default values can be queried with `/cvarname`.

## The `set` command family

Sets a cvar to a value. If the cvar doesn't exist, it gets created.

Syntax: set cvarname value

| Command | Flag        | Explanation                             |
| :------ | :---------- | :-------------------------------------- |
| set     | none        |                                         |
| seta    | archive     | Save to the config.                     |
| setu    | user info   | Sent to server on connect or change.    |
| sets    | server info | Sent in response to front end requests. |

## The `vstr` command

Executes the content of a variable as a command.

Syntax: vstr cvarname

Example #1: with `set rage quit`, `vstr rage` will execute `quit`

Example #2: Connection scripts.

In a read-only config: `set roboty connect roboty.cpmpickup.eu:27961;password lobstarz`. In-game: `vstr roboty` to connect to the server

Example #3: Smiley with alternating colors.  
`set smiley1 "say ^5:);set smiley vstr smiley2"`  
`set smiley2 "say ^6:);set smiley vstr smiley1"`  
`set smiley vstr smiley1`  
`bind space vstr smiley`

## Standard key mapping

| Key                | Action                                                    |
| :----------------- | :-------------------------------------------------------- |
| left arrow         | Move caret to the right.                                  |
| right arrow        | Move caret to the left.                                   |
| up arrow           | Next command.                                             |
| down arrow         | Previous command.                                         |
| home               | Move caret at the line's first character.                 |
| end                | Move caret after the line's last character.               |
| delete             | Delete the character above the caret.                     |
| backspace          | Delete the character to the left of the caret.            |
| page up            | Scroll up 6 lines.                                        |
| page down          | Scroll down 6 lines.                                      |
| wheel up           | Scroll up 2 lines.                                        |
| wheel down         | Scroll down 2 lines.                                      |
| insert             | Toggle between the 2 insertion modes (overstrike on/off). |
| enter              | Finish the line.                                          |
| tab                | Run auto-completion.                                      |
| shift + wheel up   | Scroll up 6 lines.                                        |
| shift + wheel down | Scroll down 6 lines.                                      |
| shift + page up    | Scroll up 1 page - 1 line.                                |
| shift + page down  | Scroll down 1 page - 1 line.                              |
| shift + insert     | Paste from the clipboard.                                 |
| control + home     | Scroll up to the first line.                              |
| control + end      | Scroll down to the last line.                             |
| control + C        | Clear the input line.                                     |
| control + L        | Clear the screen (same as typing /clear.)                 |
| control + P        | Previous command.                                         |
| control + N        | Next command.                                             |
| control + V        | Paste from the clipboard.                                 |
| control + A        | Move caret at the first character.                        |
| control + E        | Move caret after the last character.                      |
| control + M        | Toggles mark mode.                                        |

## Mark mode key mapping

| Key                 | Action                                                    |
| :------------------ | :-------------------------------------------------------- |
| control + C         | Copy selected text to the clipboard and exit mark mode.   |
| enter               | Copy selected text to the clipboard and exit mark mode.   |
| left arrow          | Move caret to the right.                                  |
| right arrow         | Move caret to the left.                                   |
| up arrow            | Move caret up by 1 line.                                  |
| down arrow          | Move caret down by 1 line.                                |
| home                | Move caret at the line's first character.                 |
| end                 | Move caret after the line's last character.               |
| page up             | Scroll up 6 lines.                                        |
| page down           | Scroll down 6 lines.                                      |
| wheel up            | Scroll up 2 lines.                                        |
| wheel down          | Scroll down 2 lines.                                      |
| shift + left arrow  | Move caret to the right and update selection.             |
| shift + right arrow | Move caret to the left and update selection.              |
| shift + up arrow    | Move caret up by 1 line and update selection.             |
| shift + down arrow  | Move caret down by 1 line and update selection.           |
| shift + home        | Move caret at the first character and update selection.   |
| shift + end         | Move caret after the last character and update selection. |
| shift + page up     | Move caret up 6 lines and update selection.               |
| shift + page down   | Move caret down 6 lines and update selection.             |
