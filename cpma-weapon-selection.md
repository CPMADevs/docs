# The ch_drawWeaponSelect replacement

In CPMA 1.52, `ch_drawWeaponSelect` was removed and 2 new SuperHUD elements were added:
|SuperHUD element|Purpose|
|--|--|
|WeaponSelection|Displays the weapon selection wheel|
|WeaponSelectionName|Displays the name of the selected weapon|

The benefit is the ability to leverage SuperHUD to be able to split the name and the weapon list apart, customize the location, alignment, direction, size, font, font size, text shadow, color, fade delay, fade duration, etc.

The downside is that you have to do a tiny bit of work and define the elements in your SuperHUD config file. We provide some snippets to get you started.

## Original CPMA 1.51 look

^^SideBySide

```SuperHUD
WeaponSelectionName
{
    rect 320 358 0 0
    textalign C
    textstyle 1
    font id
    fontsize 16 16
    color 1 1 1 1
    fadedelay 1400
    time 200
    fade
}

WeaponSelection
{
    rect 316 376 32 32
    alignh C
    color 1 1 1 1
    fadedelay 1400
    time 200
    fade
}
```

!{caption="CPMA 1.51 original (4:3 aspect, broken offset)"}(/images/guides/cpma-weapon-selection/ws-151.png)

^^SideBySideEnd

> This should be a pixel-exact replica of the original, including the fact that the weapon wheel is slightly offset to the left by 4 virtual pixels. If you want to correct this,  > use `rect 320 376 32 32` in WeaponSelection.

## CPMA 1.51 look for 16:9 displays & corrected offset

^^SideBySide

```SuperHUD

WeaponSelectionName
{
    rect 320 358 0 0
    textalign C
    textstyle 1
    font id
    fontsize 12 16
    color 1 1 1 1
    fadedelay 1400
    time 200
    fade
}

WeaponSelection
{
    rect 320 376 24 32
    alignh C
    color 1 1 1 1
    fadedelay 1400
    time 200
    fade
}
```

!{caption="CPMA 1.51 fixed (16:9 aspect, fixed offset)"}(/images/guides/cpma-weapon-selection/ws-151-fixed.png)

^^SideBySideEnd

> Now it's starting to look like something!

## CPMA 1.52 default HUD

^^SideBySide

```SuperHUD
WeaponSelectionName
{
    rect 320 458 0 0
    textalign C
    textstyle 0
    font cpma
    fontsize 5 7
    color 1 1 1 0.5
    fadedelay 1400
    time 200
    fade
}

WeaponSelection
{
    rect 320 390 14 18
    alignh C
    color 1 1 1 1
    fadedelay 1400
    time 200
    fade
}
```

!{caption="CPMA 1.52 default (16:9 aspect)"}(/images/guides/cpma-weapon-selection/ws-152.png)

^^SideBySideEnd

> Note that while the new default SuperHUD defines both elements, they are disabled (commented out with `#`).
